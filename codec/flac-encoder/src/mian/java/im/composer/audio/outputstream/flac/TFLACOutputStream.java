package im.composer.audio.outputstream.flac;

import java.io.IOException;

import org.tritonus.share.sampled.file.TDataOutputStream;

import net.sourceforge.javaflacencoder.FLACOutputStream;

public class TFLACOutputStream implements FLACOutputStream {

	private final TDataOutputStream dataOutputStream;
	private long pos = 0;

	public TFLACOutputStream(TDataOutputStream dataOutputStream) {
		this.dataOutputStream = dataOutputStream;
	}

	public void close() throws IOException {
		dataOutputStream.close();
	}

	@Override
	public long seek(long pos) throws IOException {
		if(dataOutputStream.supportsSeek()){
			dataOutputStream.seek(pos);
			return pos;
		}else{
			return this.pos;
		}
	}

	@Override
	public int write(byte[] data, int offset, int count) throws IOException {
		if(count<1){
			return 0;
		}
		dataOutputStream.write(data, offset, count);
		pos += count;
		return count;
	}

	@Override
	public long size() {
		try {
			return dataOutputStream.length();
		} catch (IOException e) {
			return -1;
		}
	}

	@Override
	public void write(byte data) throws IOException {
		dataOutputStream.write(data);
		pos++;
	}

	@Override
	public boolean canSeek() {
		return dataOutputStream.supportsSeek();
	}

	@Override
	public long getPos() {
		return pos;
	}

}

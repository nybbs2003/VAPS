package im.composer.audio.outputstream.flac;

import javax.sound.sampled.AudioFileFormat.Type;

import org.tritonus.share.sampled.file.AudioOutputStream;

import im.composer.audio.outputstream.AbstractAudioBuilder;
import net.sourceforge.javaflacencoder.StreamConfiguration;

public class FLACBuilder extends AbstractAudioBuilder {

	public static final Type TYPE = new Type("FLAC", "flac");

	@Override
	public Type getType() {
		return TYPE;
	}

	@Override
	public AudioOutputStream build() {
		int min_block_size = StreamConfiguration.MIN_BLOCK_SIZE;
		int max_block_size = StreamConfiguration.MAX_BLOCK_SIZE;
		if(map.containsKey("min_block_size")){
			min_block_size = Integer.parseInt(map.get("min_block_size").toString());
		}
		if(map.containsKey("max_block_size")){
			max_block_size = Integer.parseInt(map.get("max_block_size").toString());
		}
		return new FLACAudioOutputStream(dataOutputStream, frameCount, audioFormat,min_block_size,max_block_size);
	}

}

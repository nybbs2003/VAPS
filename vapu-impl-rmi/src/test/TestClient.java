package test;

import im.composer.generators.Sine;
import im.composer.media.sound.render.SourcePlayer;
import im.composer.vapu.rmi.in.sender.RemoteAudioSender;
import im.composer.vapu.rmi.in.sender.RemoteAudioSendingUnit;
import im.composer.vapu.rmi.out.receiver.RemoteAudioReceivingUnit;
import im.composer.vapu.rmi.out.sender.RemoteMulticastClientList;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import org.jaudiolibs.audioservers.AudioConfiguration;
import org.jaudiolibs.audioservers.AudioServer;
import org.jaudiolibs.audioservers.javasound.JSAudioServerProvider;

public class TestClient {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Throwable{
		Registry registry = LocateRegistry.getRegistry("localhost");
		RemoteMulticastClientList client_list = (RemoteMulticastClientList) registry.lookup("echo_server");
		RemoteAudioReceivingUnit rcv = new RemoteAudioReceivingUnit();
		client_list.add(rcv);
		RemoteAudioSender sender = new RemoteAudioSender(registry, "test_rcv");
		RemoteAudioSendingUnit sending_unit = new RemoteAudioSendingUnit(sender);
		AudioConfiguration context = new AudioConfiguration(48000, 2, 2, 512, true);
		SourcePlayer player = new SourcePlayer();
		player.addSource(rcv);
		player.addSource(sending_unit);
		player.configure(context);
//		RedirectedAudioIn in = player.createAudioIn(0,1);
		Sine in = new Sine();
		in.configure(context);
		sending_unit.addSource(in);
		AudioServer server = new JSAudioServerProvider().createServer(context,player);
		server.run();
	}

}

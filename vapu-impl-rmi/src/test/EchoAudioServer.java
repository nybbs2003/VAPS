package test;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.jaudiolibs.audioservers.AudioConfiguration;

import im.composer.media.sound.render.SourcePlayer;
import im.composer.vapu.rmi.codec.bzip2.BZIP2FloatSampleDecoder;
import im.composer.vapu.rmi.codec.bzip2.BZIP2FloatSampleEncoder;
import im.composer.vapu.rmi.in.receiver.RemoteAudioReceiver;
import im.composer.vapu.rmi.in.receiver.RemoteAudioReceiverImpl;
import im.composer.vapu.rmi.in.receiver.RmiOutPeer;
import im.composer.vapu.rmi.out.sender.RemoteMulticastClient.ClientState;
import im.composer.vapu.rmi.out.sender.RemoteMulticastServer;

public class EchoAudioServer {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Throwable{
		SourcePlayer player = new SourcePlayer();
		RmiOutPeer peer = new RmiOutPeer();
		player.addSource(peer);
		RemoteAudioReceiver rcv = new RemoteAudioReceiverImpl();
		peer.setAudioReceiver(rcv);
		rcv.setDecoder(new BZIP2FloatSampleDecoder());
		rcv.setEncoder(new BZIP2FloatSampleEncoder());
		rcv.setName("test_rcv");
		Registry registry = LocateRegistry.createRegistry(1099);
		registry.rebind("test_rcv", rcv);
		ExecutorService es = Executors.newCachedThreadPool();
		RemoteMulticastServer server = new RemoteMulticastServer(player,registry,es);
		AudioConfiguration context = new AudioConfiguration(48000, 2, 2, 512, true);
		server.setAudioContext(context);
		server.rebind("echo_server");
		server.setState(ClientState.PLAYING);
		server.run();
	}

}

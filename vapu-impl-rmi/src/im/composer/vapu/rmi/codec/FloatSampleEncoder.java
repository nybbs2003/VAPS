package im.composer.vapu.rmi.codec;

import im.composer.vapu.rmi.TimedFloatSampleBuffer;

import java.io.IOException;

public interface FloatSampleEncoder extends java.io.Serializable {

	byte[] encode(TimedFloatSampleBuffer buf) throws IOException;
}

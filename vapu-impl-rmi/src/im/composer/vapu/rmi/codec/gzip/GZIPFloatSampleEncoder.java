package im.composer.vapu.rmi.codec.gzip;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.zip.GZIPOutputStream;

import org.tritonus.share.sampled.FloatSampleBuffer;

import im.composer.vapu.rmi.TimedFloatSampleBuffer;
import im.composer.vapu.rmi.codec.FloatSampleEncoder;

public class GZIPFloatSampleEncoder implements FloatSampleEncoder {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6101873615896043042L;

	@Override
	public byte[] encode(TimedFloatSampleBuffer buf) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream(getSizeInBytes(buf.getBuffer()));
		GZIPOutputStream gos = new GZIPOutputStream(baos);
		ObjectOutputStream oos = new ObjectOutputStream(gos);
		oos.writeObject(buf);
		oos.flush();
		oos.close();
		gos.flush();
		gos.close();
		return baos.toByteArray();
	}
	
	private int getSizeInBytes(FloatSampleBuffer buf){
		return buf.getChannelCount() * buf.getSampleCount() * 4;
	}

}

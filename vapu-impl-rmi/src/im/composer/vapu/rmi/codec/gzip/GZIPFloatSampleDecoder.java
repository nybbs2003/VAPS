package im.composer.vapu.rmi.codec.gzip;

import im.composer.vapu.rmi.TimedFloatSampleBuffer;
import im.composer.vapu.rmi.codec.FloatSampleDecoder;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.zip.GZIPInputStream;

public class GZIPFloatSampleDecoder implements FloatSampleDecoder {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1218473308915037207L;

	@Override
	public TimedFloatSampleBuffer decode(byte[] arr) throws IOException {
		ByteArrayInputStream bais = new ByteArrayInputStream(arr);
		GZIPInputStream gis = new GZIPInputStream(bais);
		ByteArrayOutputStream baos = new ByteArrayOutputStream(arr.length*2);
		while(gis.available()>0){
			int a = gis.available();
			byte[] b = new byte[a];
			gis.read(b);
			baos.write(b);
		}
		bais = new ByteArrayInputStream(baos.toByteArray());
		ObjectInputStream ois = new ObjectInputStream(bais);
		try {
			TimedFloatSampleBuffer fsb = (TimedFloatSampleBuffer) ois.readObject();
			return fsb;
		} catch (ClassNotFoundException e) {
		}
		return null;
	}

}

package im.composer.vapu.rmi.codec;

import im.composer.vapu.rmi.TimedFloatSampleBuffer;

import java.io.IOException;

public interface FloatSampleDecoder extends java.io.Serializable {

	TimedFloatSampleBuffer decode(byte[] arr) throws IOException;
}

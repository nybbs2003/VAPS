package im.composer.vapu.rmi.codec.bzip2;

import im.composer.vapu.rmi.TimedFloatSampleBuffer;
import im.composer.vapu.rmi.codec.FloatSampleDecoder;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import org.itadaki.bzip2.BZip2InputStream;

public class BZIP2FloatSampleDecoder implements FloatSampleDecoder {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1218473308915037207L;

	@Override
	public TimedFloatSampleBuffer decode(byte[] arr) throws IOException {
		ByteArrayInputStream bais = new ByteArrayInputStream(arr);
		BZip2InputStream bis = new BZip2InputStream(bais,false);
		ByteArrayOutputStream baos = new ByteArrayOutputStream(arr.length*2);
		byte[] b = new byte[10240];
		while(true){
			int a = bis.read(b);
			if(a>-1){
				baos.write(b,0,a);
			}else{
				break;
			}
		}
		bis.close();
		bais = new ByteArrayInputStream(baos.toByteArray());
		ObjectInputStream ois = new ObjectInputStream(bais);
		try {
			TimedFloatSampleBuffer fsb = (TimedFloatSampleBuffer) ois.readObject();
			return fsb;
		} catch (ClassNotFoundException e) {
		}
		return null;
	}

}

package im.composer.vapu.rmi;

import org.tritonus.share.sampled.FloatSampleBuffer;

public class TimedFloatSampleBuffer {

	private long timestamp;
	private FloatSampleBuffer buffer;
	public TimedFloatSampleBuffer(long timestamp, FloatSampleBuffer buffer) {
		super();
		this.timestamp = timestamp;
		this.buffer = buffer;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	public FloatSampleBuffer getBuffer() {
		return buffer;
	}
	public void setBuffer(FloatSampleBuffer buffer) {
		this.buffer = buffer;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((buffer == null) ? 0 : buffer.hashCode());
		result = prime * result + (int) (timestamp ^ (timestamp >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TimedFloatSampleBuffer other = (TimedFloatSampleBuffer) obj;
		if (buffer == null) {
			if (other.buffer != null)
				return false;
		} else if (!buffer.equals(other.buffer))
			return false;
		if (timestamp != other.timestamp)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "TimedFloatSamplebuffer [timestamp=" + timestamp + ", buffer=" + buffer + "]";
	}
}

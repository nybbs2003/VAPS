package im.composer.vapu.rmi.in.sender;

import im.composer.audio.engine.Source;
import im.composer.vapu.VAPU;
import jass.engine.SinkIsFullException;

import org.tritonus.share.sampled.FloatSampleBuffer;

public class RemoteAudioSendingUnit extends VAPU {

	private final RemoteAudioSender sender;

	public RemoteAudioSendingUnit(RemoteAudioSender sender) {
		super();
		this.sender = sender;
	}

	@Override
	public synchronized Object addSource(Source s, boolean p) throws SinkIsFullException {
		if (!getSources().isEmpty()) {
			throw new SinkIsFullException();
		}
		return super.addSource(s, p);
	}

	@Override
	public void close() {
		try {
			sender.close();
		} catch (Exception e) {
		}
	}

	@Override
	protected void executeCommand(long timeStamp, String command) {

	}

	@Override
	protected void computeBuffer() {
		if (getSources().isEmpty()) {
			return;
		}
		Source src = getSources().get(0);
		try {
			FloatSampleBuffer buf = src.getBuffer(getTime());
			sender.sendAudio(getTime(), buf);
		} catch (Exception e) {
		}
	}

}

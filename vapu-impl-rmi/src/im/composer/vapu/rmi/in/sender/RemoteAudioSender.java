package im.composer.vapu.rmi.in.sender;

import im.composer.vapu.rmi.TimedFloatSampleBuffer;
import im.composer.vapu.rmi.codec.FloatSampleEncoder;
import im.composer.vapu.rmi.in.receiver.RemoteAudioReceiver;

import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.registry.Registry;

import org.tritonus.share.sampled.FloatSampleBuffer;

public class RemoteAudioSender implements AutoCloseable{

	private final Registry registry;
	private final String name;
	private final RemoteAudioReceiver receiver;

	public RemoteAudioSender(Registry registry, String name) throws IOException, NotBoundException {
		super();
		this.registry = registry;
		this.name = name;
		receiver = (RemoteAudioReceiver) registry.lookup(name);
	}
	public void sendAudio(long timestamp,FloatSampleBuffer buf) throws IOException{
		TimedFloatSampleBuffer tfsb = new TimedFloatSampleBuffer(timestamp, buf);
		FloatSampleEncoder encoder = receiver.getEncoder();
		byte[] arr = encoder.encode(tfsb);
		receiver.offer(arr);
	}
	@Override
	public void close() throws Exception {
		registry.unbind(name);
	}
}

package im.composer.vapu.rmi.in.receiver;

import im.composer.vapu.rmi.codec.FloatSampleDecoder;
import im.composer.vapu.rmi.codec.FloatSampleEncoder;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RemoteAudioReceiver extends Remote {

	void setName(String name) throws RemoteException;

	String getName() throws RemoteException;

	void setDecoder(FloatSampleDecoder decoder) throws RemoteException;

	FloatSampleDecoder getDecoder() throws RemoteException;

	void setEncoder(FloatSampleEncoder decoder) throws RemoteException;

	FloatSampleEncoder getEncoder() throws RemoteException;

	void offer(byte[] b) throws RemoteException;

	void clear() throws RemoteException;

	byte[] poll() throws RemoteException;
}

package im.composer.vapu.rmi.in.receiver;

import java.io.IOException;
import java.rmi.NotBoundException;
import java.rmi.registry.Registry;
import java.util.UUID;

public class RemoteAudioServer {

	private final Registry registry;

	public RemoteAudioServer(Registry registry) {
		super();
		this.registry = registry;
	}
	
	public String rebind(RemoteAudioReceiver receiver) throws IOException{
		String str = UUID.randomUUID().toString();
		receiver.setName(str);
		registry.rebind(str, receiver);
		return str;
	}
	
	public void unbind(String name) throws IOException, NotBoundException{
		registry.unbind(name);
	}

	public RemoteAudioReceiver lookup(String name) throws IOException, NotBoundException {
		return (RemoteAudioReceiver) registry.lookup(name);
	}

	public String[] list() throws IOException {
		return registry.list();
	}
	
	public RmiOutPeer createOutPeer(String name) throws IOException, NotBoundException{
		RemoteAudioReceiver rev = lookup(name);
		RmiOutPeer peer = new RmiOutPeer();
		peer.setAudioReceiver(rev);
		return peer;
	}
}

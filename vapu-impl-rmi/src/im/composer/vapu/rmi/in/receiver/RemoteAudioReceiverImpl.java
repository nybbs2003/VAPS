package im.composer.vapu.rmi.in.receiver;

import im.composer.vapu.rmi.codec.FloatSampleDecoder;
import im.composer.vapu.rmi.codec.FloatSampleEncoder;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.LinkedList;
import java.util.Queue;

public class RemoteAudioReceiverImpl extends UnicastRemoteObject implements RemoteAudioReceiver {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8808188832203456406L;
	private final Queue<byte[]> queue = new LinkedList<byte[]>();
	private FloatSampleDecoder decoder;
	private FloatSampleEncoder encoder;
	private String name;

	public RemoteAudioReceiverImpl() throws RemoteException {
		super();
	}

	public FloatSampleDecoder getDecoder() throws RemoteException {
		return decoder;
	}

	public void setDecoder(FloatSampleDecoder decoder) throws RemoteException {
		this.decoder = decoder;
	}

	public FloatSampleEncoder getEncoder() throws RemoteException {
		return encoder;
	}

	public void setEncoder(FloatSampleEncoder encoder) throws RemoteException {
		this.encoder = encoder;
	}

	@Override
	public void offer(byte[] b) throws RemoteException {
		queue.offer(b);
	}

	@Override
	public byte[] poll() throws RemoteException {
		return queue.poll();
	}

	@Override
	public void clear() throws RemoteException {
		queue.clear();
	}

	public String getName() throws RemoteException {
		return name;
	}

	public void setName(String name) throws RemoteException {
		this.name = name;
	}

}

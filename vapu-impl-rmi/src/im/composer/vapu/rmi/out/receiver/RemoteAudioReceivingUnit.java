package im.composer.vapu.rmi.out.receiver;

import im.composer.audio.engine.Out;
import im.composer.vapu.rmi.TimedFloatSampleBuffer;
import im.composer.vapu.rmi.out.sender.RemoteMulticastClient;

import java.nio.FloatBuffer;
import java.rmi.RemoteException;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.jaudiolibs.audioservers.AudioConfiguration;
import org.tritonus.share.sampled.AudioUtils;
import org.tritonus.share.sampled.FloatSampleBuffer;

public class RemoteAudioReceivingUnit extends Out implements RemoteMulticastClient {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2265061588045780504L;
	private final BlockingQueue<TimedFloatSampleBuffer> queue = new LinkedBlockingQueue<TimedFloatSampleBuffer>();
	private FloatSampleBuffer _cur_buf;// only used in doTalkingBuffer();
	private TimedFloatSampleBuffer _cur_tfsb;// only used in doRealBuffer();
	private ClientState state = ClientState.STOP;

	public RemoteAudioReceivingUnit() {
	}

	public RemoteAudioReceivingUnit(AudioConfiguration context) {
		super(context);
	}

	@Override
	public synchronized void configure(AudioConfiguration context) throws Exception {
		super.configure(context);
		queue.clear();
	}

	@Override
	public void setState(ClientState state) throws RemoteException {
		this.state = state;
		if (state != this.state) {
			queue.clear();
			_cur_buf = null;
		}
	}

	@Override
	public void audioReceived(long time, List<FloatBuffer> channels, int nframes) throws RemoteException {
		FloatSampleBuffer buf = toBuffer(channels, nframes);
		TimedFloatSampleBuffer tfsb = new TimedFloatSampleBuffer(time, buf);
		queue.offer(tfsb);
	}

	private FloatSampleBuffer toBuffer(List<FloatBuffer> channels, int nframes) {
		FloatSampleBuffer buf = new FloatSampleBuffer(channels.size(), nframes, getContext().getSampleRate());
		for (int i = 0; i < channels.size(); i++) {
			float[] src = channels.get(i).array();
			float[] dest = buf.getChannel(i);
			System.arraycopy(src, 0, dest, 0, nframes);
		}
		return buf;
	}

	@Override
	protected void computeBuffer() {
		switch (state) {
		case TALKING:
			doTalkingBuffer();
			break;
		case PLAYING:
		case RECORDING:
			doRealBuffer();
			break;
		default:
			break;
		}
	}

	private void doTalkingBuffer() {
		int pos = 0;
		if (_cur_buf != null) {
			buf.mix(_cur_buf, 0, 0, _cur_buf.getSampleCount());
		}
		while (pos <= getBufferSize()) {
			TimedFloatSampleBuffer _tfsb = queue.poll();
			if (_tfsb == null) {
				_cur_buf = null;
				break;
			}
			FloatSampleBuffer _buf = _tfsb.getBuffer();
			int len = Math.min(getBufferSize() - pos, _buf.getSampleCount());
			buf.mix(_buf, 0, pos, len);
			if (len < _buf.getSampleCount()) {
				_cur_buf = new FloatSampleBuffer(_buf.getChannelCount(), len, getContext().getSampleRate());
				_cur_buf.mix(_buf, pos, 0, len);
				break;
			}
			pos += len;
		}
	}

	private void doRealBuffer() {
		if (queue.isEmpty()) {
			return;
		} else if (queue.element().getBuffer().getSampleCount() <= getBufferSize()) {
			doMergeBuffer();
		} else {
			doDevideBuffer();
		}
	}

	private void doMergeBuffer() {
		double nanosecond_per_frame = AudioUtils.frames2MillisD(getBufferSize(), getContext().getSampleRate()) * Math.pow(10, 6);
		double t1 = nanosecond_per_frame * getTime();
		int pos = 0;
		while (!queue.isEmpty()) {
			TimedFloatSampleBuffer _tfsb;
			if (_cur_tfsb != null) {
				_tfsb = _cur_tfsb;
			} else {
				_tfsb = queue.poll();
			}
			double t2 = _tfsb.getTimestamp();
			if (t2 + nanosecond_per_frame < t1) {
				_cur_tfsb = null;
				continue;
			}
			int _pos = (int) ((t2 - t1) / nanosecond_per_frame * getBufferSize());
			int limit = getBufferSize() - pos;
			FloatSampleBuffer _buf = _tfsb.getBuffer();
			int len = Math.min(_buf.getSampleCount(), limit);
			if (_pos < 0) {
				int pos1 = _buf.getSampleCount() + _pos;
				len = Math.min(len, _buf.getSampleCount() - pos1);
				buf.mix(_buf, pos1, 0, len);
			} else {
				buf.mix(_buf, 0, pos, len);
				if (len < _buf.getSampleCount()) {
					FloatSampleBuffer _buf1 = new FloatSampleBuffer(_buf.getChannelCount(), len, getContext().getSampleRate());
					_buf1.mix(_buf1, _buf.getSampleCount() - len, 0, len);
					_cur_tfsb = new TimedFloatSampleBuffer(_tfsb.getTimestamp(), _buf);
					break;
				}
			}
			pos += len;
			_cur_tfsb = null;
		}
	}

	private void doDevideBuffer() {
		throw new UnsupportedOperationException("Not Implemented Yet!");
	}

}

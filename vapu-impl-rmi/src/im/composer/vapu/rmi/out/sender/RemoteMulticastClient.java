package im.composer.vapu.rmi.out.sender;

import java.io.Serializable;
import java.nio.FloatBuffer;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import org.jaudiolibs.audioservers.AudioConfiguration;

public interface RemoteMulticastClient extends Remote, Serializable {

	public void configure(AudioConfiguration context) throws Exception;

	public void setState(ClientState state) throws RemoteException;

	public void audioReceived(long time, List<FloatBuffer> channels, int nframes) throws RemoteException;

	public static enum ClientState {
		/**
		 * 停止状态，不播放任何音频，不调用<code>audioReceived</code>方法。调用后时间跳回初始时间。
		 */
		STOP,
		/**
		 * 聊天状态，不播放音乐，回放所有参与者的音频输入，不要求时间同步，<code>timestamp</code>永远为负。
		 */
		TALKING,
		/**
		 * 准备播放，所有客户端确认后正式开始播放。
		 */
		PREPARED_TO_PLAY,
		/**
		 * 播放状态，回放所有参与者的音频输入，要求时间同步，播放<code>AudioClient</code>的输出。
		 */
		PLAYING,
		/**
		 * 暂停状态，回放所有参与者的音频输入，不要求时间同步，不播放<code>AudioClient</code>的输出，
		 * <code>timestamp</code>永远为负。 设置为此状态的瞬间，客户端保持当前播放位置不变。
		 */
		PAUSED,
		/**
		 * 准备录音，所有客户端确认后正式开始录音。
		 */
		PREPARED_TO_RECORD,
		/**
		 * 录音状态，回放所有参与者的音频输入，要求时间同步，播放<code>AudioClient</code>的输出。
		 */
		RECORDING
	}
}

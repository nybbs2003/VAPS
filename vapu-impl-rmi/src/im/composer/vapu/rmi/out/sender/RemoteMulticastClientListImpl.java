package im.composer.vapu.rmi.out.sender;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class RemoteMulticastClientListImpl extends UnicastRemoteObject implements RemoteMulticastClientList {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6339291609557511192L;
	private final List<RemoteMulticastClient> list = Collections.synchronizedList(new LinkedList<RemoteMulticastClient>());

	public RemoteMulticastClientListImpl() throws RemoteException {
	}

	@Override
	public int size() throws RemoteException {
		return list.size();
	}

	@Override
	public boolean add(RemoteMulticastClient e) throws RemoteException {
		return list.add(e);
	}

	@Override
	public boolean remove(RemoteMulticastClient o) throws RemoteException {
		return list.remove(o);
	}

	@Override
	public List<RemoteMulticastClient> getList() throws RemoteException {
		return Collections.unmodifiableList(list);
	}

}

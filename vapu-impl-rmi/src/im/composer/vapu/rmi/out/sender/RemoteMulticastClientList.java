package im.composer.vapu.rmi.out.sender;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface RemoteMulticastClientList extends Remote{

	public abstract List<RemoteMulticastClient> getList() throws RemoteException;

	public abstract boolean remove(RemoteMulticastClient o) throws RemoteException;

	public abstract boolean add(RemoteMulticastClient e) throws RemoteException;

	public abstract int size() throws RemoteException;

}

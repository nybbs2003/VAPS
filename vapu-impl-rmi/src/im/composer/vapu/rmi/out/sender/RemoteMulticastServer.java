package im.composer.vapu.rmi.out.sender;

import im.composer.vapu.rmi.out.sender.RemoteMulticastClient.ClientState;

import java.nio.FloatBuffer;
import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import org.jaudiolibs.audioservers.AudioClient;
import org.jaudiolibs.audioservers.AudioConfiguration;
import org.jaudiolibs.audioservers.AudioServer;
import org.tritonus.share.sampled.AudioUtils;

public class RemoteMulticastServer implements AudioServer {

	private final Registry registry;
	private final RemoteMulticastClientList client_list;
	private String name;
	private AudioConfiguration audioContext;
	private final ExecutorService es;
	private final AudioClient client;
	private ClientState state = ClientState.STOP;

	public RemoteMulticastServer(AudioClient client, Registry registry, ExecutorService es) throws RemoteException {
		this.client = client;
		this.registry = registry;
		this.es = es;
		client_list = new RemoteMulticastClientListImpl();
	}

	public void rebind(String name) throws RemoteException, AccessException {
		if (name == null) {
			throw new java.lang.IllegalArgumentException("Name may not be null!");
		}
		this.name = name;
		registry.rebind(name, client_list);
	}

	public void unbind() throws RemoteException, NotBoundException, AccessException {
		if (name == null) {
			throw new NotBoundException("Not Bound Yet!");
		}
		registry.unbind(name);
	}

	@Override
	public void run() throws Exception {
		if (state == ClientState.TALKING || state == ClientState.PLAYING || state == ClientState.RECORDING) {
			configureClients();
			long microsecond_per_frame = (long) (AudioUtils.frames2MillisD(audioContext.getMaxBufferSize(), audioContext.getSampleRate()) * 1000);
			long timestamp = state == ClientState.TALKING ? -1 : 0;
			long diff = System.nanoTime();
			while (isActive()) {
				if(timestamp>=0){
					timestamp = System.nanoTime() - diff;
				}
				pushAudio(timestamp);
				TimeUnit.MICROSECONDS.sleep(microsecond_per_frame);
			}
		} else {
			throw new java.lang.IllegalStateException("ClientState must be either TALKING, PLAYING or RECORDING !");
		}
	}

	private void configureClients() throws Exception {
		client.configure(getAudioContext());
		for (final RemoteMulticastClient client : client_list.getList()) {
			es.submit(new Callable<Void>() {
				@Override
				public Void call() throws Exception {
					client.configure(getAudioContext());
					return null;
				}
			});
		}
	}

	private void pushAudio(long timestamp) throws RemoteException {
		List<FloatBuffer> inputs = new ArrayList<>();
		List<FloatBuffer> outputs = new ArrayList<>();
		for (int i = 0; i < audioContext.getInputChannelCount(); i++) {
			inputs.add(FloatBuffer.allocate(audioContext.getMaxBufferSize()));
		}
		for (int i = 0; i < audioContext.getOutputChannelCount(); i++) {
			outputs.add(FloatBuffer.allocate(audioContext.getMaxBufferSize()));
		}
		client.process(timestamp, inputs, outputs, audioContext.getMaxBufferSize());
		doOutput(timestamp, outputs, audioContext.getMaxBufferSize());
	}

	private void doOutput(final long timestamp, final List<FloatBuffer> outputs, final int frames) throws RemoteException {
		for (final RemoteMulticastClient client : client_list.getList()) {
			es.submit(new Callable<Void>() {
				@Override
				public Void call() throws Exception {
					client.audioReceived(timestamp, outputs, frames);
					return null;
				}
			});
		}
	}

	public void setAudioContext(AudioConfiguration audioContext) {
		this.audioContext = audioContext;
	}

	@Override
	public AudioConfiguration getAudioContext() {
		return audioContext;
	}

	@Override
	public boolean isActive() {
		return state != ClientState.STOP;
	}

	@Override
	public void shutdown() {
		try {
			setState0(ClientState.STOP);
		} catch (RemoteException e) {
		}
	}

	public ClientState getState() {
		return state;
	}

	public void setState(ClientState state) {
		try {
			setState0(state);
		} catch (RemoteException e) {
		}
	}

	private void setState0(final ClientState state) throws RemoteException {
		for (final RemoteMulticastClient client : client_list.getList()) {
			es.submit(new Callable<Void>() {

				@Override
				public Void call() throws Exception {
					client.setState(state);
					return null;
				}
			});
		}
		this.state = state;
	}
}

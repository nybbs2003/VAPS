package im.composer.vapu.adapter.audioops;

import org.jaudiolibs.audioops.AudioOp;
import org.jaudiolibs.audioservers.AudioConfiguration;

import im.composer.audio.engine.Source;
import im.composer.vapu.VAPU;
import jass.engine.BufferNotAvailableException;
import jass.engine.SinkIsFullException;

public class OpAdapter extends VAPU {
	
	private final AudioOp op;

	public OpAdapter(AudioOp op) {
		if(op==null){
			throw new NullPointerException();
		}
		this.op = op;
	}

	@Override
	public synchronized void configure(AudioConfiguration context) throws Exception {
		super.configure(context);
		op.initialize(context.getSampleRate(), getBufferSize());
	}

	@Override
	public synchronized Object addSource(Source s, boolean p) throws SinkIsFullException {
		if(!getSources().isEmpty()){
			throw new SinkIsFullException();
		}
		return super.addSource(s, p);
	}

	public AudioOp getOp() {
		return op;
	}

	@Override
	protected void executeCommand(long timeStamp, String command) {

	}

	@Override
	protected void computeBuffer() {
		if(getSources().isEmpty()){
			return;
		}
		try {
			float[][] inputs = getSources().get(0).getBuffer().getAllChannels();
			float[][] outputs = buf.getAllChannels();
			op.processReplace(bufferSize, outputs, inputs);
			int nChannels = buf.getChannelCount();
			for(int i=0;i<nChannels;i++){
				buf.setRawChannel(i, outputs[i]);
			}
		} catch (BufferNotAvailableException e) {
			return;
		}
	}

}

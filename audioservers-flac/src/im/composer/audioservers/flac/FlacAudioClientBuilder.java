package im.composer.audioservers.flac;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class FlacAudioClientBuilder {

	private final ServerSocket ss;
	public FlacAudioClientBuilder(int port) throws IOException {
		ss = new ServerSocket(port);
	}
	
	public FlacAudioClient buildClient() throws IOException{
		Socket socket = ss.accept();
		return new FlacAudioClient(socket);
	}

	public void close() throws IOException {
		ss.close();
	}
}

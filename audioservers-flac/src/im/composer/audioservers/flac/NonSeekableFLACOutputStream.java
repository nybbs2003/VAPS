package im.composer.audioservers.flac;

import java.io.IOException;
import java.io.OutputStream;

import javaFlacEncoder.FLACOutputStream;

public class NonSeekableFLACOutputStream implements FLACOutputStream {

	private final OutputStream out;
	private long pos = 0;

	public NonSeekableFLACOutputStream(OutputStream out) {
		super();
		this.out = out;
	}

	@Override
	public void close() throws IOException {
		out.close();
	}

	@Override
	public long seek(long pos) throws IOException {
		return this.pos;
	}

	@Override
	public int write(byte[] data, int offset, int count) throws IOException {
		out.write(data, offset, count);
		pos += count;
		return count;
	}

	@Override
	public long size() {
		return pos;
	}

	@Override
	public void write(byte data) throws IOException {
		out.write(data);
		pos++;
	}

	@Override
	public boolean canSeek() {
		return false;
	}

	@Override
	public long getPos() {
		return pos;
	}

}

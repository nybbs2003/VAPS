package cc.gpai.data_stru.multimap;

import java.util.NavigableMap;

public interface NavigableMultiMap<K, V> extends MultiMap<K, V>, NavigableMap<K, V> {

	NavigableMultiMap<K, V> subMultiMap(K fromKey, boolean fromInclusive, K toKey, boolean toInclusive);
}

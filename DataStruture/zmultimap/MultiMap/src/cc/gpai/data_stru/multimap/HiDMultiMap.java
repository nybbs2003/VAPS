package cc.gpai.data_stru.multimap;

import java.util.Collection;
import java.util.List;

public interface HiDMultiMap<K, V> extends MultiMap<K, V>, HiDMap<K, V> {

	public Collection<V> getValues(List<K> key);

	public Collection<V> getValues(K... key);

	public Collection<V> put(Collection<V> value, K... key);

	public Collection<V> put(K[] key, Collection<V> value);

	public Collection<V> put(List<K> key, Collection<V> value);

	public Collection<V> put(K[] key, V... value);

}

/*
 *  Copyright 2011 David Zhang (nybbs2003@163.com)
 * 
 *  This file is part of MultiMap.
 *
 *  MultiMap is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 3 only 
 *  as published by the Free Software Foundation.
 *
 *  MultiMap is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with MultiMap.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package cc.gpai.data_stru.multimap.example;
import cc.gpai.data_stru.multimap.DMap;
import cc.gpai.data_stru.multimap.MultiDMap;
import cc.gpai.data_stru.multimap.MultiMap;
import cc.gpai.data_stru.multimap.impl.HashDMap;
import cc.gpai.data_stru.multimap.impl.HashSetMultiMap;
import cc.gpai.data_stru.multimap.impl.LinkedHashMultiDMap;


@SuppressWarnings("rawtypes")
public class MultiMapExample {

	/**
	 * @param args
	 */
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		DMap dmap = new HashDMap();
		dmap.put("123", "abc");
		dmap.put("456", "def");
		String str  = (String) dmap.getKey("def");
		System.out.println(str);
		MultiMap mmap = new HashSetMultiMap();
		mmap.put("123", "a");
		mmap.put("123", "b");
		mmap.put("123", "c");
		for(Object o:mmap.getValues("123")){
			System.out.print(o+"\t");
		}
		System.out.println();
		MultiDMap mdmap = new LinkedHashMultiDMap();
		mdmap.put("China", "123");
		mdmap.put("China", "456");
		mdmap.put("America", "abc");
		mdmap.put("America", "def");
		mdmap.put("Russia", "123");
		mdmap.put("Russia", "456");
		for(Object o:mdmap.getValues("China")){
			System.out.print(o+"\t");
		}
		System.out.println();
		for(Object o:mdmap.getKeys("123")){
			System.out.print(o+"\t");
		}
	}

}

/*
 *  Copyright 2011 David Zhang (nybbs2003@163.com)
 * 
 *  This file is part of MultiMap.
 *
 *  MultiMap is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License version 3 only 
 *  as published by the Free Software Foundation.
 *
 *  MultiMap is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with MultiMap.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package cc.gpai.data_stru.multimap;

import java.util.AbstractMap;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import cc.gpai.data_stru.multimap.impl.EntryImpl;


public abstract class AbstractMultiMap<K, V> extends AbstractMap<K, V> implements MultiMap<K, V> {

	private Map<K, Collection<V>> map;

	protected final Map<K, Collection<V>> getMap() {
		if(map==null){
			map = newMap();
		}
		return map;
	}

	@Override
	public int size() {
		return getMap().size();
	}

	@Override
	public boolean isEmpty() {
		return getMap().isEmpty();
	}

	@Override
	public boolean containsKey(Object key) {
		return getMap().containsKey(key);
	}

	@Override
	public boolean containsValue(Object value) {
		for (Collection<V> c : getMap().values()) {
			if (c.contains(value))
				return true;
		}
		return false;
	}

	@Override
	public V get(Object key) {
		Collection<V> col = getMap().get(key);
		if(col==null)
			return null;
		else
			return col.iterator().next();
	}

	@Override
	public Collection<V> getValues(Object key) {
		Collection<V> col = getMap().get(key);
		if(col==null)return newCollection();
		return col;
	}

	private void ensureKey(K key){
		if(!getMap().containsKey(key))
			getMap().put(key, newCollection());
	}
	@Override
	public Collection<V> putValues(K key, Collection<V> value) {
		Collection<V> v = getValues(key);
		ensureKey(key);
		getMap().get(key).addAll(value);
		return v;
	}

	@Override
	public V put(K key, V value) {
		V v = get(key);
		ensureKey(key);
		getMap().get(key).add(value);
		return v;
	}
	protected abstract Collection<V> newCollection();
	protected abstract Map<K,Collection<V>> newMap();

	@Override
	public V remove(Object key) {
		V v = get(key);
		getMap().remove(key);
		return v;
	}

	@Override
	public boolean remove(K key, V value) {
		return getValues(key).remove(value);
	}

	@Override
	public boolean removeKey(K key) {
		boolean removed = false;
		if(getMap().containsKey(key)){
			Collection<V> col = getMap().get(key);
			if(col!=null){
				removed = !col.isEmpty();
				col.clear();
			}
			getMap().remove(key);
		}
		return removed;
	}

	@Override
	public boolean removeValue(V value) {
		if(value==null){
			return false;
		}
		boolean removed = false;
		for(Collection<V> col:getMap().values()){
			while(col.contains(value)){
				col.remove(value);
				removed = true;
			}
		}
		return removed;
	}

	@Override
	public void clear() {
		getMap().clear();
	}

	@Override
	public Set<K> keySet() {
		return getMap().keySet();
	}

	@Override
	public Set<Entry<K, V>> entrySet() {
		Set<Entry<K, V>> set = new HashSet<Entry<K, V>>();
		Set<Entry<K, Collection<V>>> es = getMap().entrySet();
		for (Entry<K, Collection<V>> entry : es) {
			K key = entry.getKey();
			Collection<V> values = entry.getValue();
			for (V value : values) {
				set.add(new EntryImpl<K, V>(key, value));
			}
		}
		return set;
	}
}

package cc.gpai.data_stru.multimap;

import java.util.Collection;
import java.util.Comparator;
import java.util.Map;
import java.util.NavigableMap;
import java.util.NavigableSet;
import java.util.SortedMap;

import cc.gpai.data_stru.multimap.impl.EntryImpl;
import cc.gpai.data_stru.multimap.impl.MultiMapImpl;

public abstract class AbstractNavigableMultiMap<K, V> extends MultiMapImpl<K, V> implements NavigableMultiMap<K, V> {

	public AbstractNavigableMultiMap(Class<? extends Map> map_clz, Class<? extends Collection> col_clz) {
		super(map_clz, col_clz);
	}

	protected NavigableMap<K,Collection<V>> getNavigableMap(){
		return (NavigableMap<K, Collection<V>>) getMap();
	}
	
	@Override
	public Entry<K, V> lowerEntry(K key) {
		return new EntryImpl<K, V>(lowerKey(key),get(lowerKey(key)));
	}

	@Override
	public K lowerKey(K key) {
		return getNavigableMap().lowerKey(key);
	}

	@Override
	public Entry<K, V> floorEntry(K key) {
		return new EntryImpl<K, V>(floorKey(key),get(floorKey(key)));
	}

	@Override
	public K floorKey(K key) {
		return getNavigableMap().floorKey(key);
	}

	@Override
	public Entry<K, V> ceilingEntry(K key) {
		return new EntryImpl<K, V>(ceilingKey(key),get(ceilingKey(key)));
	}

	@Override
	public K ceilingKey(K key) {
		return getNavigableMap().ceilingKey(key);
	}

	@Override
	public Entry<K, V> higherEntry(K key) {
		return new EntryImpl<K, V>(higherKey(key),get(higherKey(key)));
	}

	@Override
	public K higherKey(K key) {
		return getNavigableMap().higherKey(key);
	}

	@Override
	public java.util.Map.Entry<K, V> firstEntry() {
		return new EntryImpl<K, V>(firstKey(),get(firstKey()));
	}

	@Override
	public Entry<K, V> lastEntry() {
		return new EntryImpl<K, V>(lastKey(),get(lastKey()));
	}

	@Override
	public java.util.Map.Entry<K, V> pollFirstEntry() {
		throw new UnsupportedOperationException();
	}

	@Override
	public java.util.Map.Entry<K, V> pollLastEntry() {
		throw new UnsupportedOperationException();
	}

	@Override
	public NavigableMap<K, V> descendingMap() {
		throw new UnsupportedOperationException();
	}

	@Override
	public NavigableSet<K> navigableKeySet() {
		return getNavigableMap().navigableKeySet();
	}

	@Override
	public NavigableSet<K> descendingKeySet() {
		return getNavigableMap().descendingKeySet();
	}

	@Override
	public NavigableMap<K, V> subMap(K fromKey, boolean fromInclusive, K toKey, boolean toInclusive) {
		throw new UnsupportedOperationException();
	}

	@Override
	public NavigableMap<K, V> headMap(K toKey, boolean inclusive) {
		throw new UnsupportedOperationException();
	}

	@Override
	public NavigableMap<K, V> tailMap(K fromKey, boolean inclusive) {
		throw new UnsupportedOperationException();
	}

	@Override
	public SortedMap<K, V> subMap(K fromKey, K toKey) {
		throw new UnsupportedOperationException();
	}

	@Override
	public SortedMap<K, V> headMap(K toKey) {
		throw new UnsupportedOperationException();
	}

	@Override
	public SortedMap<K, V> tailMap(K fromKey) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Comparator<? super K> comparator() {
		return getNavigableMap().comparator();
	}

	@Override
	public K firstKey() {
		return getNavigableMap().firstKey();
	}

	@Override
	public K lastKey() {
		return getNavigableMap().lastKey();
	}

	@Override
	public NavigableMultiMap<K, V> subMultiMap(K fromKey, boolean fromInclusive, K toKey, boolean toInclusive) {
		throw new UnsupportedOperationException();
	}

}

package cc.gpai.data_stru.multimap.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import cc.gpai.data_stru.multimap.AbstractHiDMultiMap;

public class LinkedHashHiDMultiMap<K, V> extends AbstractHiDMultiMap<K, V> {

	@Override
	protected List<K> newKeyList(List<K> l) {
		return new LinkedList<K>(l);
	}

	@Override
	protected Collection<V> newValueCollection(Collection<V> col) {
		return new LinkedList<V>(col);
	}

	@Override
	protected Map<List<K>, Collection<V>> initMap() {
		return new HashMap<List<K>, Collection<V>>();
	}

}

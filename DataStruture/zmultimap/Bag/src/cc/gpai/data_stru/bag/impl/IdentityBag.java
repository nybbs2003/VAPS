package cc.gpai.data_stru.bag.impl;

import java.util.IdentityHashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import cc.gpai.data_stru.bag.MapBag;

public class IdentityBag<E> extends MapBag<E> implements java.io.Serializable {

	/**
     * 
     */
	private static final long serialVersionUID = 3854521749296882095L;

	public IdentityBag() {
	}

	@Override
	protected Map<E, AtomicLong> createMap() {
		return new IdentityHashMap<E, AtomicLong>();
	}

}

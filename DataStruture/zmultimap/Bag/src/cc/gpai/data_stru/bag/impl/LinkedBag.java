package cc.gpai.data_stru.bag.impl;

import cc.gpai.data_stru.bag.ListBag;
import java.util.LinkedList;
import java.util.List;

public class LinkedBag<E> extends ListBag<E> implements java.io.Serializable {

	/**
     * 
     */
	private static final long serialVersionUID = 5901246053587746729L;

	public LinkedBag() {
	}

	@Override
	protected List<BagEntry<E>> createList() {
		return new LinkedList<BagEntry<E>>();
	}

}

package cc.gpai.data_stru.bag.impl;

import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.atomic.AtomicLong;

import cc.gpai.data_stru.bag.MapBag;

public class TreeBag<E> extends MapBag<E> implements java.io.Serializable {
	/**
     * 
     */
	private static final long           serialVersionUID = 6032476997320851542L;
	private final Comparator<? super E> comparator;

	public TreeBag() {
		this(null);
	}

	public TreeBag(Comparator<? super E> comparator) {
		this.comparator = comparator;
	}

	@Override
	public Map<E, AtomicLong> createMap() {
		return new TreeMap<E, AtomicLong>(comparator);
	}

}

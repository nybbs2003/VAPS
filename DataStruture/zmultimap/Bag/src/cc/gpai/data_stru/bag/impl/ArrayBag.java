package cc.gpai.data_stru.bag.impl;

import java.util.ArrayList;
import java.util.List;

import cc.gpai.data_stru.bag.ListBag;

public class ArrayBag<E> extends ListBag<E> implements java.io.Serializable {

	/**
     * 
     */
    private static final long serialVersionUID = -2961482631922724552L;

	public ArrayBag() {
	}

	@Override
	protected List<BagEntry<E>> createList() {
		return new ArrayList<BagEntry<E>>();
	}

}

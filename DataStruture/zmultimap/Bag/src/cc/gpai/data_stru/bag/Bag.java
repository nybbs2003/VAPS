package cc.gpai.data_stru.bag;

import java.util.Collection;
import java.util.Set;

public interface Bag<E> extends Collection<E> {

	/**
	 * Change the number of element <code>e</code>, adding <code>count</code> times.
	 * 
	 * @param e
	 * @param count
	 * @return the updated number of <code>e</code> in this <code>Bag</code>.
	 */
	public long modCount(E e, long count);

	/**
	 * Returns the number of specfied element contained in this bag.
	 * 
	 * @param obj
	 * @return
	 */
	public long getCount(Object obj);

	/**
	 * Sets the number of specfied element contained in this bag to <code>count</code>.
	 * 
	 * @param e
	 * @param count
	 */
	public void setCount(E e, long count);

	/**
	 * Returns a {@link Set} view of the elements contained in this bag. The set is backed by the bag, so changes to the
	 * bag are reflected in the set, and vice-versa. If the bag is modified while an iteration over the set is in
	 * progress (except through the iterator's own <tt>remove</tt> operation), the results of the iteration are
	 * undefined. The set supports element removal, which removes the corresponding mapping from the map, via the
	 * <tt>Iterator.remove</tt>, <tt>Set.remove</tt>, <tt>removeAll</tt>, <tt>retainAll</tt>, and <tt>clear</tt>
	 * operations. It does support the <tt>add</tt> or <tt>addAll</tt> operations as well.
	 * 
	 * @return a set view of the elements contained in this bag
	 */
	public Set<E> uniqueSet();

}

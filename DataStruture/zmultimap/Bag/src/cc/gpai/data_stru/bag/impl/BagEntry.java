package cc.gpai.data_stru.bag.impl;

import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicLong;

public class BagEntry<E> implements Entry<E, AtomicLong>, java.io.Serializable {

	/**
     * 
     */
	private static final long serialVersionUID = 8784414541231061464L;
	private final E           key;
	private final AtomicLong  value            = new AtomicLong(1);

	public BagEntry(E key) {
		this.key = key;
	}

	public BagEntry(E key, Number value) {
		this(key);
		this.value.set(value.longValue());
	}

	public E getKey() {
		return key;
	}

	public AtomicLong getValue() {
		return value;
	}

	/**
	 * Use getValue().set(long val)
	 */
	@Deprecated
	public AtomicLong setValue(AtomicLong value) {
		long old = this.value.getAndSet(value.get());
		return new AtomicLong(old);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(key);
		builder.append(" = ");
		builder.append(value);
		return builder.toString();
	}

}
package cc.gpai.data_stru.bag.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import cc.gpai.data_stru.bag.MapBag;

public class HashBag<E> extends MapBag<E> implements java.io.Serializable {

	/**
     * 
     */
	private static final long serialVersionUID = -5611380702412499160L;

	@Override
	public Map<E, AtomicLong> createMap() {
		return new HashMap<E, AtomicLong>();
	}

}

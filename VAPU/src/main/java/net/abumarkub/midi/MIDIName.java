package net.abumarkub.midi;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.TreeMap;

public class MIDIName {

	private static final Map<Integer, String> INSTRUMENT_NAME = new TreeMap<>();
	private static final Map<Integer, String> PERCUSSION_NAME = new TreeMap<>();
	private static final String[] NOTE_NAME = new String[]{"C","C#","D","D#","E","F","F#","G","G#","A","A#","B"};
	

	static {
		try {
			readData("inst_name.txt",INSTRUMENT_NAME);
		} catch (Exception e) {
		}
		try {
			readData("perc_name.txt",PERCUSSION_NAME);
		} catch (Exception e) {
		}
	}
	
	private final static void readData(String fn, Map<Integer, String> map) throws Exception{
		BufferedReader reader = new BufferedReader(new InputStreamReader(Thread.currentThread().getContextClassLoader().getResourceAsStream(MIDIName.class.getPackage().getName().replace('.', '/')+"/"+fn)));
		while(reader.ready()){
			String[] line = reader.readLine().split(" ",2);
			int num = Integer.parseInt(line[0]);
			map.put(num, line[1]);
		}
	}

	public static final String getInstrumentName(int i) {
		return INSTRUMENT_NAME.get(i);
	}

	public static final String getPercussionName(int i) {
		return PERCUSSION_NAME.get(i);
	}
	
	public static final String getNoteName(int i){
		return NOTE_NAME[i%12]+(i/12);
	}
}

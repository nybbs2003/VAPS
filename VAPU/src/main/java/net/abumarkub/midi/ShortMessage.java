/*!
 *  copyright 2012 abudaan http://abumarkub.net
 *  code licensed under MIT 
 *  http://abumarkub.net/midibridge/license
 * 
 *  based on above code, modified by Super-User <zdl@zdl.hk>
 */

package net.abumarkub.midi;

import javax.sound.midi.InvalidMidiDataException;

public class ShortMessage extends javax.sound.midi.ShortMessage {

	public ShortMessage() {
		super();
	}

	public ShortMessage(byte[] data) {
		super(data);
	}

	public ShortMessage(int command, int channel, int data1, int data2) throws InvalidMidiDataException {
		super(command, channel, data1, data2);
	}

	public ShortMessage(int status, int data1, int data2) throws InvalidMidiDataException {
		super(status, data1, data2);
	}

	public ShortMessage(int status) throws InvalidMidiDataException {
		super(status);
	}

	@Override
	public String toString() {
		StringBuilder message = new StringBuilder();

		if (getCommand() == 176) {
			message.append("CMD:");
			message.append(MIDIData.getCommand(getCommand()));
			message.append(" CHAN:");
			message.append(getChannel());
			message.append(" ");
			message.append(MIDIData.getControlChangeMessage(getData1()));
			message.append(":");
			message.append(getData2());
//			message.append(" | STATUS:");
//			message.append(getStatus());
			// message.append(" TIME:");
			// message.append(timeStamp);
		} else if (getCommand() == 128 || getCommand() == 144) {
			message.append("CMD:");
			message.append(MIDIData.getCommand(getCommand()));
			message.append(" CHAN:");
			message.append(getChannel());
			message.append(" NOTE:");
			if(getChannel()==9){
				message.append(MIDIName.getPercussionName(getData1()));
			}else{
				message.append(MIDIName.getNoteName(getData1()));				
			}
			message.append(" VELOCITY:");
			message.append(getData2());
//			message.append(" | STATUS:");
//			message.append(getStatus());
			// message.append(" TIME:");
			// message.append(timeStamp);
		} else {
			message.append("CMD:");
			message.append(MIDIData.getCommand(getCommand()));
			message.append(" CHAN:");
			message.append(getChannel());
			if(getCommand()==192){
				message.append(" INSTRUMENT:");
				message.append(MIDIName.getInstrumentName(getData1()));
			}else{
				message.append(" DATA1:");
				message.append(getData1());
				message.append(" DATA2:");
				message.append(getData2());				
			}
//			message.append(" | STATUS:");
//			message.append(getStatus());
			// message.append(" TIME:");
			// message.append(timeStamp);
		}

		return message.toString();
	}

}
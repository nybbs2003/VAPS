package themidibus.listener;

import javax.sound.midi.MidiChannel;

public interface ChannelBasedMidiListener extends MidiListener {

	MidiChannel getChannel(int channel);
}

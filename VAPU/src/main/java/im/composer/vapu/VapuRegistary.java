package im.composer.vapu;

import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;

public final class VapuRegistary {

	
	private VapuRegistary() {
	}

	private static Map<UUID,VAPU> map = Collections.synchronizedMap(new TreeMap<UUID,VAPU>());

	public static VAPU get(UUID key) {
		return map.get(key);
	}

	public static void put(UUID key, VAPU value) {
		if(value==null){
			map.remove(key);
		}
		map.put(key, value);
	}

	public static void remove(UUID key) {
		map.remove(key);
	}
	public static void remove(VAPU value) {
		for(Map.Entry<UUID,VAPU> e:map.entrySet()){
			if(e.getValue().equals(value)){
				map.remove(e.getKey());
				break;
			}
		}
	}
}

package im.composer.audio.component;

import im.composer.audio.engine.Source;

import java.util.List;

import org.tritonus.share.sampled.FloatSampleBuffer;

/**
 * 多音源混音器，buffer_size和sample_rate必须相同
 * @author david
 *
 */
public interface SourcesMixer {

	void setSources(List<Source> list);
    void setTime(long t);
	FloatSampleBuffer getMixedBuffer();
}

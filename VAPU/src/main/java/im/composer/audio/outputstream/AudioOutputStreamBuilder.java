package im.composer.audio.outputstream;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;

import org.tritonus.share.sampled.file.AudioOutputStream;
import org.tritonus.share.sampled.file.TDataOutputStream;

public interface AudioOutputStreamBuilder {

	AudioFileFormat.Type getType();

	AudioOutputStream build();

	AudioOutputStreamBuilder set(String key, Object value);

	AudioOutputStreamBuilder setFormat(AudioFormat audioFormat);

	AudioOutputStreamBuilder setFrameCount(long frameCount);

	AudioOutputStreamBuilder setTarget(TDataOutputStream dataOutputStream);
}

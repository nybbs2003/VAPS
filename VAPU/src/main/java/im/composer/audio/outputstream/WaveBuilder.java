package im.composer.audio.outputstream;

import javax.sound.sampled.AudioFileFormat.Type;

import org.tritonus.sampled.file.WaveAudioOutputStream;
import org.tritonus.share.sampled.file.AudioOutputStream;

public class WaveBuilder extends AbstractAudioBuilder {
	
	@Override
	public AudioOutputStream build() {
		return new WaveAudioOutputStream(audioFormat, frameCount, dataOutputStream);
	}

	@Override
	public Type getType() {
		return Type.WAVE;
	}

}

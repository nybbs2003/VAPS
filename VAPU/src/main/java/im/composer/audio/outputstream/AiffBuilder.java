package im.composer.audio.outputstream;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFileFormat.Type;

import org.tritonus.sampled.file.AiffAudioOutputStream;
import org.tritonus.share.sampled.file.AudioOutputStream;

public class AiffBuilder extends AbstractAudioBuilder {

	@Override
	public AudioOutputStream build() {
		return new AiffAudioOutputStream(audioFormat, AudioFileFormat.Type.AIFF, frameCount, dataOutputStream);
	}

	@Override
	public Type getType() {
		return Type.AIFF;
	}


}

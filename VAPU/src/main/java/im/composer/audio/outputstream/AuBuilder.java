package im.composer.audio.outputstream;

import javax.sound.sampled.AudioFileFormat.Type;

import org.tritonus.sampled.file.AuAudioOutputStream;
import org.tritonus.share.sampled.file.AudioOutputStream;

public class AuBuilder extends AbstractAudioBuilder {

	@Override
	public AudioOutputStream build() {
		return new AuAudioOutputStream(audioFormat, frameCount, dataOutputStream);
	}

	@Override
	public Type getType() {
		return Type.AU;
	}

}

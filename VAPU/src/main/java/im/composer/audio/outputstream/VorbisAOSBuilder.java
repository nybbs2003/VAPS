package im.composer.audio.outputstream;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFileFormat.Type;

import org.tritonus.share.sampled.file.AudioOutputStream;

import im.composer.media.sound.codec.FloatOutputStream;

public class VorbisAOSBuilder extends AbstractAudioBuilder {

	@Override
	public Type getType() {
		return new AudioFileFormat.Type("Vorbis", "ogg");
	}

	@Override
	public AudioOutputStream build() {
		float base_quality = 0.7f;
		if (audioFormat.getProperty("quality") !=null) {
			int quality = Integer.parseInt(audioFormat.getProperty("quality").toString());
			if (quality < 0) {
				quality = 0;
			} else if (quality > 100) {
				quality = 100;
			}
			base_quality = quality / 100;
		}
		return new FloatOutputStream(new VorbisAudioOutputStream(audioFormat, frameCount, dataOutputStream, base_quality), audioFormat, -1);
	}

}

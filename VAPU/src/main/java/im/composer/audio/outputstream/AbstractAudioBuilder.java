package im.composer.audio.outputstream;

import java.util.NavigableMap;
import java.util.TreeMap;

import javax.sound.sampled.AudioFormat;

import org.tritonus.share.sampled.file.TDataOutputStream;

public abstract class AbstractAudioBuilder implements AudioOutputStreamBuilder {

	protected final NavigableMap<String,Object> map = new TreeMap<>();
	protected long frameCount = -1;
	protected AudioFormat audioFormat;
	protected TDataOutputStream dataOutputStream;
	
	@Override
	public AudioOutputStreamBuilder set(String key, Object value) {
		map.put(key, value);
		return this;
	}

	@Override
	public AudioOutputStreamBuilder setFormat(AudioFormat audioFormat) {
		this.audioFormat=audioFormat;
		return this;
	}

	@Override
	public AudioOutputStreamBuilder setFrameCount(long frameCount) {
		this.frameCount=frameCount;
		return this;
	}

	@Override
	public AudioOutputStreamBuilder setTarget(TDataOutputStream dataOutputStream) {
		this.dataOutputStream=dataOutputStream;
		return this;
	}
}

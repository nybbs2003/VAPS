package im.composer.audio.outputstream;

import java.io.IOException;
import java.util.Objects;

import javax.sound.sampled.AudioFormat;

import org.tritonus.share.sampled.file.AudioOutputStream;
import org.tritonus.share.sampled.file.TDataOutputStream;

public abstract class AbstractAudioOutputStream implements AudioOutputStream {
	protected final TDataOutputStream dataOutputStream;
	private final AudioFormat format;
	private final long length;

	public AbstractAudioOutputStream(TDataOutputStream dataOutputStream, AudioFormat format, long length) {
		Objects.requireNonNull(dataOutputStream);
		Objects.requireNonNull(format);
		this.dataOutputStream = dataOutputStream;
		this.format = format;
		this.length = length;
	}

	@Override
	public AudioFormat getFormat() {
		return format;
	}

	@Override
	public long getLength() {
		return length;
	}

	@Override
	public void close() throws IOException {
		dataOutputStream.close();
	}

}

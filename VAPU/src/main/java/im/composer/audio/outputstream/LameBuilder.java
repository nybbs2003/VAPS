package im.composer.audio.outputstream;

import javax.sound.sampled.AudioFileFormat.Type;

import org.tritonus.share.sampled.file.AudioOutputStream;

import im.composer.media.audio.stream.LameOutputStream;

public class LameBuilder extends AbstractAudioBuilder {

	public static final Type TYPE = new Type("lame", "mp3");

	@Override
	public Type getType() {
		return TYPE;
	}

	@Override
	public AudioOutputStream build() {
		LameOutputStream los = new LameOutputStream(dataOutputStream);
		los.setSourceFormat(audioFormat);
		return los;
	}

}

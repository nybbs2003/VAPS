package im.composer.audio.window;


public class HannWindow implements WindowType{


	@Override
	public float[] getWindow(int width) {
		float[] arr = new float[width];
		for(int i=0;i<width;i++){
			arr[i] = (float) Math.sin(Math.PI*i/(width-1));
		}
		return arr;
	}

}

package im.composer.audio.window;

public interface WindowType {

	float[] getWindow(int width);
}

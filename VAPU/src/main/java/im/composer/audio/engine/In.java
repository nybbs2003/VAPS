package im.composer.audio.engine;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

import jass.engine.SinkIsFullException;

/**
   Input unit which contains Sources and does something with them in
   run().  Maintains Vector of Sources. Needs only implementation of run(). 
   @author Kees van den Doel (kvdoel@cs.ubc.ca)
*/

public abstract class In implements Sink {
    protected Vector<Source> sourceContainer = new Vector<Source>();

    /** add source to Sink.
        @param s Source to add.
        @return object representing Source in Sink (may be null).
    */
    public synchronized Object addSource(Source s) throws SinkIsFullException {
        sourceContainer.addElement(s);
        return null;
    }

    /** Remove Source.
        @param s Source to remove.
    */
    public synchronized void removeSource(Source s) {
        sourceContainer.removeElement(s);
    }

    @Override
	public void removeSource(int i) {
    	sourceContainer.remove(i);
	}

	public List<Source> getSources() {
        return Collections.unmodifiableList(sourceContainer);
    }
}


package im.composer.audio.engine;
import java.util.List;

import jass.engine.SinkIsFullException;

/**
   Interface for objects containing  multiple Sources.
   This object recieves input lines.
   @author Kees van den Doel (kvdoel@cs.ubc.ca)
*/

public interface Sink {
    /** Add a Source
        @param s Source to add
        @return Object associated with the source (may be a controller, or whatever)
    */    
    Object addSource(Source s) throws SinkIsFullException;
    
    /** Remove a Source
        @param s Source to remove
    */
    void removeSource(Source s);
    
    void removeSource(int i);

    /** Get list of sources.
        @return unmodifiable list of the Sources, empty if there are none.
    */
    List<Source> getSources();
}

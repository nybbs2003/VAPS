package im.composer.audio.engine;

import jass.engine.BufferNotAvailableException;

import org.jaudiolibs.audioservers.AudioConfiguration;
import org.tritonus.share.sampled.FloatSampleBuffer;

/**
   Interface defining a source. This produces audio-rate buffers. Has
   buffer with audio data and a time concept . 
   @author Kees van den Doel (kvdoel@cs.ubc.ca)
   @author David Zhang (zdl@zdl.hk)
*/

public interface Source {


	/**
	 * 设置本输出单元的音频环境。此方法抛出异常，应当于初始化本输出单元后调用。
	 * 初始化后，音频环境若不容许变更，应当抛出<code>java.lang.IlleagalStateException</code>异常。
	 * 若容许音频环境发生变更，但是在设置过程中发生错误，则应当抛出其他异常。
	 */
	public void configure(AudioConfiguration context) throws Exception;
	
    /** Get buffer with timestamp t.
        @param t timestamp of buffer. For example, a frame index.
    */
    FloatSampleBuffer getBuffer(long t) throws BufferNotAvailableException;

    /** Get buffer, whatever happens to be available. (Generally intended
        not to trigeer a computation.
    */
    FloatSampleBuffer getBuffer() throws BufferNotAvailableException;

    /** Get current time.
        @return current time.
    */
    long getTime();
    
    /** Set current time.
        @param t current time.
    */
    void setTime(long t);

    /** Get buffer size.
        @return buffer size in samples.
    */
    int getBufferSize();
    
    /** Set buffer size.
        @param bufferSize buffer size.
    */
    void setBufferSize(int bufferSize);

    /** Clears buffer to zero.
     */
    void clearBuffer();
}


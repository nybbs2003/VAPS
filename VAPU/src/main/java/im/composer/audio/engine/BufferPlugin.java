package im.composer.audio.engine;

import org.jaudiolibs.audioservers.AudioConfiguration;
import org.tritonus.share.sampled.FloatSampleBuffer;

@FunctionalInterface
public interface BufferPlugin{

	public default void configure(AudioConfiguration context){}

	public void bufferReceived(FloatSampleBuffer buf, long time);

}

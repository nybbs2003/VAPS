package im.composer.audio.format;

import javax.sound.sampled.AudioFormat;

import org.jaudiolibs.audioservers.AudioConfiguration;

public class AudioConfFormat extends AudioFormat {

	private final AudioConfiguration conf;

	public AudioConfFormat(AudioConfiguration conf, int sampleSizeInBits, int channels, boolean signed, boolean bigEndian) {
		super(conf.getSampleRate(), sampleSizeInBits, channels, signed, bigEndian);
		this.conf = conf;
	}

	public AudioConfiguration getConf() {
		return conf;
	}

}

package im.composer.audio.util;

import im.composer.misc.util.CharSequenceIterator;

import java.util.Map;
import java.util.TreeMap;

public class PitchParser {

	private static final Map<Character, Integer> map = new TreeMap<>();
	static {
		map.put('C', 0);
		map.put('D', 2);
		map.put('E', 4);
		map.put('F', 5);
		map.put('G', 7);
		map.put('A', 9);
		map.put('H', 9);
		map.put('B', 11);
		map.put('c', 0);
		map.put('d', 2);
		map.put('e', 4);
		map.put('f', 5);
		map.put('g', 7);
		map.put('a', 9);
		map.put('h', 9);
		map.put('b', 11);
	}

	public int parse(CharSequence q) {
		int key = 0, octave = 0, accd = 0;
		for (char c : new CharSequenceIterator(q)) {
			switch (c) {
			case 'C':
			case 'D':
			case 'E':
			case 'F':
			case 'G':
			case 'A':
			case 'H':
			case 'B':
			case 'c':
			case 'd':
			case 'e':
			case 'f':
			case 'g':
			case 'a':
			case 'h':
			case 'b':
				key = map.get(c);
				break;
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				octave = c - '0';
				break;
			case '#':
			case '\u266F':
				accd = 1;
				break;
			case '\u266D':
				accd = -1;
				break;
			}
		}
		return (octave+1) * 12 + key + accd;
	}
}

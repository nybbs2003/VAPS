package im.composer.audio.util;

import java.nio.FloatBuffer;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.tritonus.share.sampled.FloatSampleBuffer;

public abstract class FloatBufferUtil {

	public static final List<FloatBuffer> toFloatBufferList(FloatSampleBuffer fsb) {
		return Stream.of(fsb.getAllChannels()).map(o->FloatBuffer.wrap(o)).collect(Collectors.toList());
	}

	public static final FloatSampleBuffer toFloatSampleBuffer(List<FloatBuffer> list, float sampleRate) {
		int sampleCount = list.stream().mapToInt(o->o.limit()).max().getAsInt();
		FloatSampleBuffer fsb = new FloatSampleBuffer(list.size(), sampleCount, sampleRate);
		for (int i = 0; i < list.size(); i++) {
			FloatBuffer f = list.get(i);
			f.position(0);
			f.get(fsb.getChannel(i));
			f.position(0);
		}
		return fsb;
	}
}

package im.composer.audio.util;

public class ToneCents {

	public static final double diffInCents(double freq1, double freq2){
		return Math.log(freq2 / freq1) / Math.log(2)  * 1200d;
	}
}

package im.composer.controller;

import java.util.SortedSet;
import java.util.TreeSet;

public class ControllerRecorder extends TreeSet<ControlRecordEntry> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5234752015987893195L;
	private transient long startRecordTimeMill;
	private boolean recording = false;

	public void startRecording() {
		startRecordTimeMill = System.currentTimeMillis();
		recording = true;
	}

	public void stopRecording() {
		recording = false;
	}

	public boolean isRecording() {
		return recording;
	}

	public void recordCommand(long timeStamp, String command) {
		if (timeStamp < 0) {
			timeStamp = System.currentTimeMillis() - startRecordTimeMill;
		}
		if (recording) {
			add(new ControlRecordEntry(timeStamp, command));
		}
	}

	public SortedSet<ControlRecordEntry> subSet(long fromElement, long toElement) {
		return super.subSet(new ControlRecordEntry(fromElement), new ControlRecordEntry(toElement));
	}

}

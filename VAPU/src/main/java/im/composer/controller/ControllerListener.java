package im.composer.controller;


public interface ControllerListener {

	public abstract void setListener(ControllerListener listener);

	public abstract ControllerListener getListener();
	
	/**
	 * 收到某种控制信息
	 * @param timeStamp 毫秒，相对于乐曲起始时间。若为负数，则表示不支持timeStamp，该命令应当立即执行。
	 * @param command
	 */
	public abstract void commandReceived(long timeStamp, String command);
	
	public abstract String getProperty(String key);

	public abstract void setProperty(String key, String value);

	public abstract String[] listCapabilities();

	public abstract boolean canDo(String cap);
}

package im.composer.controller;

public class ControlRecordEntry implements Comparable<ControlRecordEntry>{

	/**
	 * millisecond
	 */
	public final long timeStamp;
	public final String command;
	public ControlRecordEntry(long timeStamp) {
		this(timeStamp,null);
	}
	public ControlRecordEntry(long timeStamp, String command) {
		if(command==null){
			command = "";
		}
		this.timeStamp = timeStamp;
		this.command = command;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((command == null) ? 0 : command.hashCode());
		result = prime * result + (int) (timeStamp ^ (timeStamp >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ControlRecordEntry other = (ControlRecordEntry) obj;
		if (command == null) {
			if (other.command != null)
				return false;
		} else if (!command.equals(other.command))
			return false;
		if (timeStamp != other.timeStamp)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "ControlRecordEntry [timeStamp=" + timeStamp + ", command=" + command + "]";
	}
	@Override
	public int compareTo(ControlRecordEntry o) {
		int comp = (int) Math.signum(timeStamp-o.timeStamp);
		if(comp==0){
			comp = command.compareTo(o.command);
		}
		return comp;
	}
}

package im.composer.media.sound.codec;

import java.io.IOException;
import java.nio.file.Path;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.UnsupportedAudioFileException;

public interface PathAwareAudioFileReader {
	public abstract AudioFileFormat getAudioFileFormat(Path path) throws UnsupportedAudioFileException, IOException;
    public abstract AudioInputStream getAudioInputStream(Path path) throws UnsupportedAudioFileException, IOException;

}

package im.composer.media.sound.codec;

public interface CommentSupport {

	public boolean addComment(String key, String value);
}

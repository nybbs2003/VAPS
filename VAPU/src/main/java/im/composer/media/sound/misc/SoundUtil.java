package im.composer.media.sound.misc;

public class SoundUtil {

	
	public static void applyVolumeLimit(float[] sample,float min,float max){
		for(int i=0;i<sample.length;i++){
			float f = sample[i];
			if(f>max){
				f = max;
			}else if(f<min){
				f = min;
			}
			sample[i]=f;
		}
	}
}

package im.composer.media.sound.render;

import java.nio.FloatBuffer;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.IntStream;

import org.jaudiolibs.audioservers.AudioClient;
import org.jaudiolibs.audioservers.AudioConfiguration;
import org.tritonus.share.sampled.AudioUtils;
import org.tritonus.share.sampled.FloatSampleBuffer;

import im.composer.audio.engine.BufferPlugin;
import im.composer.audio.engine.In;
import im.composer.audio.engine.Out;
import im.composer.generators.RedirectedAudioIn;
import jass.engine.BufferNotAvailableException;

public class SourcePlayer extends In implements AudioClient {

	protected AudioConfiguration context;
	protected transient FloatSampleBuffer buffer;
	private Map<int[], RedirectedAudioIn> channels_audioin = new HashMap<>();
	private transient long t;// time
	private float duration = Float.POSITIVE_INFINITY;
	protected transient double millisecond_per_frame;
	protected BufferPlugin input_plugin,output_plugin;

	@Override
	public synchronized void configure(AudioConfiguration context) throws Exception {
		this.context = context;
		int channels = context.getOutputChannelCount();
		int bufz = context.getMaxBufferSize();
		buffer = new FloatSampleBuffer(channels, bufz, context.getSampleRate());
		millisecond_per_frame = AudioUtils.frames2MillisD(context.getMaxBufferSize(), context.getSampleRate());
		setTime(0);
		confgureRedirectedAudioIn();
	}

	private void confgureRedirectedAudioIn() {
		channels_audioin.values().forEach(in -> {
			try {
				in.configure(context);
			} catch (Exception e) {
			}
		});
	}

	public long getTime() {
		return t;
	}

	public void setTime(long t) {
		this.t = t;
		sourceContainer.parallelStream().forEach(src->{
			try {
				src.configure(context);
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (src instanceof Out) {// Especially InOut
				((Out) src).resetTime(t);
			} else {
				src.setTime(t);
			}
		});
	}

	public AudioConfiguration getContext() {
		return context;
	}

	public void setContext(AudioConfiguration context) {
		this.context = context;
		try {
			configure(context);
		} catch (Exception e) {
		}
	}

	public float getDuration() {
		return duration;
	}

	public void setDuration(float duration) {
		this.duration = duration;
	}

	public RedirectedAudioIn createAudioIn(int... channels) {
		if (channels.length < 1) {
			throw new IllegalArgumentException();
		}
		RedirectedAudioIn result = channels_audioin.get(channels);
		if (result == null) {
			result = new RedirectedAudioIn();
			result.setChannels(channels.length);
			if (context != null) {
				try {
					result.configure(context);
				} catch (Exception e) {
				}
			}
			channels_audioin.put(channels, result);
		}
		return result;
	}

	public void removeAudioIn(RedirectedAudioIn in) {
		Optional<Entry<int[], RedirectedAudioIn>> opt = channels_audioin.entrySet().stream().filter(e->e.getValue().equals(in)).findFirst();
		if(opt.isPresent()){
			channels_audioin.remove(opt.get().getKey());
		}
	}

	public void setInputPlugin(BufferPlugin input_plugin) {
		this.input_plugin = input_plugin;
	}

	public void setOutputPlugin(BufferPlugin output_plugin) {
		this.output_plugin = output_plugin;
	}

	@Override
	public boolean process(long time, List<FloatBuffer> inputs, List<FloatBuffer> outputs, int nframes) {
		if(input_plugin!=null){
			FloatSampleBuffer fsb = new FloatSampleBuffer(inputs.size(),context.getMaxBufferSize(),context.getSampleRate());
			for(int i=0;i<inputs.size();i++){
				fsb.setRawChannel(i, inputs.get(i).array());
			}
			input_plugin.bufferReceived(fsb, t);
		}
		try {
			doInput(inputs);
		} catch (Exception e) {
		}
		try {
			doOutput(outputs);
		} catch (BufferNotAvailableException e) {
		}
		if(output_plugin!=null){
			output_plugin.bufferReceived(buffer, t);
		}
		if (millisecond_per_frame * t > duration) {
			return false;
		}
		return true;
	}

	protected void doInput(List<FloatBuffer> inputs) throws Exception {
		channels_audioin.forEach((channels, in) -> {
			FloatSampleBuffer buf = in.getBuffer();
			for (int i : channels) {
				if (i > inputs.size() || i > buf.getChannelCount()) {
					continue;
				}
				float[] arr = inputs.get(i).array();
				buf.setRawChannel(i, arr);
			}
		});
	}

	protected void doOutput(List<FloatBuffer> outputs) throws BufferNotAvailableException {
		getNextBuffer();
		int channels = Math.min(buffer.getChannelCount(), outputs.size());
		IntStream.range(0, channels).parallel().forEach(i -> {
			float[] data = buffer.getChannel(i);
			FloatBuffer buf = outputs.get(i);
			buf.clear();
			buf.put(data);
		});

	}

	private synchronized void getNextBuffer() throws BufferNotAvailableException {
		buffer.makeSilence();
		sourceContainer.parallelStream().map(src -> {
			try {
				return src.getBuffer(t);
			} catch (Exception e) {
				return null;
			}
		}).filter(buf -> buf != null).forEach(buf -> buffer.mix(buf));
		t++;
	}

	@Override
	public void shutdown() {

	}

}

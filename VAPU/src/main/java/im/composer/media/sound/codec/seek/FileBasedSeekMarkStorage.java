package im.composer.media.sound.codec.seek;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class FileBasedSeekMarkStorage implements SeekMarkStorage {

	protected Path getStorageFilePath(Path path){
		return path.getFileSystem().provider().getPath(URI.create(path.toUri().toString()+".seek.bin"));
	}
	
	@Override
	public boolean seekMarkExists(Path path) {
		Path path1 = getStorageFilePath(path);
		return Files.isReadable(path1);
	}

	@Override
	public InputStream getInputStreamForSeekMark(Path path) throws IOException {
		Path path1 = getStorageFilePath(path);
		return new GZIPInputStream(Files.newInputStream(path1), 1024);
	}

	@Override
	public OutputStream getOutputStreamForSeekMark(Path path) throws IOException {
		Path path1 = getStorageFilePath(path);
		return new GZIPOutputStream(Files.newOutputStream(path1), 1024, true);
	}

}

package im.composer.media.sound.codec.seek;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Path;

public interface SeekMarkStorage {

	boolean seekMarkExists(Path path);

	InputStream getInputStreamForSeekMark(Path path) throws IOException;

	OutputStream getOutputStreamForSeekMark(Path path) throws IOException;
}

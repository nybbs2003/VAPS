package im.composer.media.sound.storage;

import java.util.Collection;
import java.util.LinkedList;
import java.util.TreeMap;

public class AudioFragmentMap extends TreeMap<Long, Collection<AudioFragment>> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4956523737429861715L;

	public synchronized void put(long frame_start_time, AudioFragment cur_rcd) {
		if(!containsKey(frame_start_time)){
			put(frame_start_time,new LinkedList<AudioFragment>());
		}
		get(frame_start_time).add(cur_rcd);
	}

}

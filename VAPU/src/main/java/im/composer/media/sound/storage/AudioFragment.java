package im.composer.media.sound.storage;

public class AudioFragment implements Comparable<AudioFragment> {

	private AudioDataSource ds;
	private long start_time;
	private long offset;
	private long length;

	/**
	 * 当前音频插入到目标的时间，单位：毫秒，以原始音频时间（1:1）计算，未处理时间伸缩。
	 */
	public long getStartTime() {
		return start_time;
	}

	public void setStartTime(long start_time) {
		this.start_time = start_time;
	}

	/**
	 * 当前资源起始时间，单位：毫秒，若为负数，则在实际起始前添加静音；若为正数，则跳过指定时间。
	 */
	public long getOffset() {
		return offset;
	}

	public void setOffset(long offset) {
		this.offset = offset;
	}

	/**
	 * 当前资源时长，单位：毫秒，以原始音频时间（1:1）计算，未处理时间伸缩。<br>
	 * 若短于资源记录时长，则截取终止以前的部分；若长于资源纪录市场，则在末位返回静音。
	 */
	public long getLength() {
		return length;
	}

	public void setLength(long length) {
		this.length = length;
	}

	public AudioDataSource getDataSource() {
		return ds;
	}

	public void setDataSource(AudioDataSource ds) {
		this.ds = ds;
	}

	/**
	 * 比较两个音频片段的起始时间。
	 */
	@Override
	public int compareTo(AudioFragment o) {
		return (int) Math.signum(start_time - o.start_time);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ds == null) ? 0 : ds.hashCode());
		result = prime * result + (int) (length ^ (length >>> 32));
		result = prime * result + (int) (offset ^ (offset >>> 32));
		result = prime * result + (int) (start_time ^ (start_time >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AudioFragment other = (AudioFragment) obj;
		if (ds == null) {
			if (other.ds != null)
				return false;
		} else if (!ds.equals(other.ds))
			return false;
		if (length != other.length)
			return false;
		if (offset != other.offset)
			return false;
		if (start_time != other.start_time)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "AudioFragment [ds=" + ds + ", start_time=" + start_time + ", offset=" + offset + ", length=" + length + "]";
	}
}

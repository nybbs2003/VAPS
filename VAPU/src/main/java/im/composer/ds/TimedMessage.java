package im.composer.ds;

import java.util.Arrays;

import javax.sound.midi.MidiMessage;

public class TimedMessage {

	private long timeStamp = -1;
	private MidiMessage message;

	public TimedMessage() {
	}

	public TimedMessage(long timeStamp, MidiMessage message) {
		super();
		this.timeStamp = timeStamp;
		this.message = message;
	}

	public long getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}

	public MidiMessage getMessage() {
		return message;
	}

	public void setMessage(MidiMessage message) {
		this.message = message;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((message == null) ? 0 : message.getMessage().hashCode());
		result = prime * result + (int) (timeStamp ^ (timeStamp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TimedMessage other = (TimedMessage) obj;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!Arrays.equals(message.getMessage(), other.message.getMessage()))
			return false;
		if (timeStamp != other.timeStamp)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TimedMessage [timeStamp=" + timeStamp + ", message=" + message + "]";
	}

}

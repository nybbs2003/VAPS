package im.composer.ds;

import java.util.TreeSet;

public class CapSet extends TreeSet<String> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6206368350401418879L;

	public CapSet() {
		super(String.CASE_INSENSITIVE_ORDER);
	}

}

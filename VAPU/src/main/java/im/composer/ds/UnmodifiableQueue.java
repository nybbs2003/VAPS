package im.composer.ds;

import java.util.Collection;
import java.util.Iterator;
import java.util.Queue;

public class UnmodifiableQueue<E> implements Queue<E> {

	private final Queue<E> queue;

	public UnmodifiableQueue(Queue<E> queue) {
		this.queue = queue;
	}

	public boolean add(E e) {
		throw new UnsupportedOperationException();
	}

	public int size() {
		return queue.size();
	}

	public boolean isEmpty() {
		return queue.isEmpty();
	}

	public boolean contains(Object o) {
		return queue.contains(o);
	}

	public boolean offer(E e) {
		throw new UnsupportedOperationException();
	}

	public Iterator<E> iterator() {
		return new Iterator<E>() {
			private final Iterator<? extends E> i = queue.iterator();

			public boolean hasNext() {
				return i.hasNext();
			}

			public E next() {
				return i.next();
			}

			public void remove() {
				throw new UnsupportedOperationException();
			}
		};
	}

	public E remove() {
		throw new UnsupportedOperationException();
	}

	public E poll() {
		return queue.poll();
	}

	public Object[] toArray() {
		return queue.toArray();
	}

	public E element() {
		return queue.element();
	}

	public E peek() {
		return queue.peek();
	}

	public <T> T[] toArray(T[] a) {
		return queue.toArray(a);
	}

	public boolean remove(Object o) {
		throw new UnsupportedOperationException();
	}

	public boolean containsAll(Collection<?> c) {
		return queue.containsAll(c);
	}

	public boolean addAll(Collection<? extends E> c) {
		throw new UnsupportedOperationException();
	}

	public boolean removeAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}

	public boolean retainAll(Collection<?> c) {
		throw new UnsupportedOperationException();
	}

	public void clear() {
		throw new UnsupportedOperationException();
	}

	@Override
	public String toString() {
		return String.valueOf(queue);
	}

}

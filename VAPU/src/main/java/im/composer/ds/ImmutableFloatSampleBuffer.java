package im.composer.ds;

import java.util.Arrays;
import java.util.Objects;

import javax.sound.sampled.AudioFormat;

import org.tritonus.share.sampled.FloatSampleBuffer;

public class ImmutableFloatSampleBuffer extends FloatSampleBuffer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4114798365123200628L;

	private final FloatSampleBuffer fsb;

	public ImmutableFloatSampleBuffer(FloatSampleBuffer fsb) {
		this.fsb = Objects.requireNonNull(fsb);
	}

	public void init(int newChannelCount, int newSampleCount, float newSampleRate) {
		throw new UnsupportedOperationException();
	}

	public void init(int newChannelCount, int newSampleCount, float newSampleRate, boolean lazy) {
		throw new UnsupportedOperationException();
	}

	public void initFromByteArray(byte[] buffer, int offset, int byteCount, AudioFormat format) {
		throw new UnsupportedOperationException();
	}

	public void initFromByteArray(byte[] buffer, int offset, int byteCount, AudioFormat format, boolean lazy) {
		throw new UnsupportedOperationException();
	}

	public void initFromFloatSampleBuffer(FloatSampleBuffer source) {
		throw new UnsupportedOperationException();
	}

	public int writeByteBuffer(byte[] buffer, int srcByteOffset, AudioFormat format, int dstSampleOffset, int aSampleCount) {
		return fsb.writeByteBuffer(buffer, srcByteOffset, format, dstSampleOffset, aSampleCount);
	}

	public void reset() {
		throw new UnsupportedOperationException();
	}

	public void reset(int newChannels, int newSampleCount, float newSampleRate) {
		throw new UnsupportedOperationException();
	}

	public int getByteArrayBufferSize(AudioFormat format) {
		return fsb.getByteArrayBufferSize(format);
	}

	public int getByteArrayBufferSize(AudioFormat format, int lenInSamples) {
		return fsb.getByteArrayBufferSize(format, lenInSamples);
	}

	public int convertToByteArray(byte[] buffer, int offset, AudioFormat format) {
		return fsb.convertToByteArray(buffer, offset, format);
	}

	public int convertToByteArray(int readOffset, int lenInSamples, byte[] buffer, int writeOffset, AudioFormat format) {
		return fsb.convertToByteArray(readOffset, lenInSamples, buffer, writeOffset, format);
	}

	public byte[] convertToByteArray(AudioFormat format) {
		return fsb.convertToByteArray(format);
	}

	public void changeSampleCount(int newSampleCount, boolean keepOldSamples) {
		if(newSampleCount!=fsb.getSampleCount()){
			throw new UnsupportedOperationException();
		}
	}

	public void makeSilence() {
		throw new UnsupportedOperationException();
	}

	public void makeSilence(int offset, int count) {
		if(count!=0){
			throw new UnsupportedOperationException();
		}
	}

	public void makeSilence(int channel) {
		throw new UnsupportedOperationException();
	}

	public void makeSilence(int channel, int offset, int count) {
		if(count!=0){
			throw new UnsupportedOperationException();
		}
	}

	public void linearFade(float startVol, float endVol) {
		throw new UnsupportedOperationException();
	}

	public void linearFade(float startVol, float endVol, int offset, int count) {
		throw new UnsupportedOperationException();
	}

	public void linearFade(int channel, float startVol, float endVol, int offset, int count) {
		throw new UnsupportedOperationException();
	}

	public void addChannel(boolean silent) {
		throw new UnsupportedOperationException();
	}

	public void insertChannel(int index, boolean silent) {
		throw new UnsupportedOperationException();
	}

	public void insertChannel(int index, boolean silent, boolean lazy) {
		throw new UnsupportedOperationException();
	}

	public void removeChannel(int channel) {
		throw new UnsupportedOperationException();
	}

	public void removeChannel(int channel, boolean lazy) {
		throw new UnsupportedOperationException();
	}

	public void copyChannel(int sourceChannel, int targetChannel) {
		throw new UnsupportedOperationException();
	}

	public void copyChannel(int sourceChannel, int sourceOffset, int targetChannel, int targetOffset, int aSampleCount) {
		if(aSampleCount!=0){
			throw new UnsupportedOperationException();
		}
	}

	public void copy(int sourceIndex, int destIndex, int length) {
		if(length!=0){
			throw new UnsupportedOperationException();
		}
	}

	public void copy(int channel, int sourceIndex, int destIndex, int length) {
		if(length!=0){
			throw new UnsupportedOperationException();
		}
	}

	public void expandChannel(int targetChannelCount) {
		throw new UnsupportedOperationException();
	}

	public void mixDownChannels() {
		throw new UnsupportedOperationException();
	}

	public void mix(FloatSampleBuffer source) {
		throw new UnsupportedOperationException();
	}

	public void mix(FloatSampleBuffer source, int sourceOffset, int thisOffset, int count) {
		if(count!=0){
			throw new UnsupportedOperationException();
		}
	}

	public int copyTo(FloatSampleBuffer dest, int destOffset, int count) {
		return fsb.copyTo(dest, destOffset, count);
	}

	public int copyTo(int srcOffset, FloatSampleBuffer dest, int destOffset, int count) {
		return fsb.copyTo(srcOffset, dest, destOffset, count);
	}

	public void setSamplesFromBytes(byte[] input, int inByteOffset, AudioFormat format, int floatOffset, int frameCount) {
		throw new UnsupportedOperationException();
	}

	public int getChannelCount() {
		return fsb.getChannelCount();
	}

	public int getSampleCount() {
		return fsb.getSampleCount();
	}

	public float getSampleRate() {
		return fsb.getSampleRate();
	}

	public void setSampleCount(int newSampleCount, boolean keepOldSamples) {
		if(newSampleCount!=fsb.getSampleCount()){
			throw new UnsupportedOperationException();
		}
	}

	public void setSampleRate(float sampleRate) {
		if(sampleRate!=fsb.getSampleRate()){
			throw new UnsupportedOperationException();
		}
	}

	public float[] getChannel(int channel) {
		return Arrays.copyOf(fsb.getChannel(channel), fsb.getSampleCount());
	}

	public float[] setRawChannel(int channel, float[] data) {
		throw new UnsupportedOperationException();
	}

	public float[][] getAllChannels() {
		float[][] arr = new float[fsb.getChannelCount()][];
		for(int i=0;i<arr.length;i++){
			arr[i] = Arrays.copyOf(fsb.getChannel(i), fsb.getSampleCount());
		}
		return arr;
	}

	public void setDitherBits(float ditherBits) {
		throw new UnsupportedOperationException();
	}

	public float getDitherBits() {
		return fsb.getDitherBits();
	}

	public void setDitherMode(int mode) {
		throw new UnsupportedOperationException();
	}

	public int getDitherMode() {
		return fsb.getDitherMode();
	}

	public FloatSampleBuffer clone() {
		return this;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((fsb == null) ? 0 : fsb.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ImmutableFloatSampleBuffer other = (ImmutableFloatSampleBuffer) obj;
		if (fsb == null) {
			if (other.fsb != null)
				return false;
		} else if (!fsb.equals(other.fsb))
			return false;
		return true;
	}

	public String toString() {
		return fsb.toString();
	}
}

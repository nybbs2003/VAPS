package im.composer.midi.seq;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.Sequence;
import javax.sound.midi.Track;

public class FreeSequence extends Sequence implements List<Track> {

	public FreeSequence(float divisionType, int resolution) throws InvalidMidiDataException {
		super(divisionType, resolution);
	}

	public FreeSequence(float divisionType, int resolution, int numTracks) throws InvalidMidiDataException {
		super(divisionType, resolution, numTracks);
	}

	public int size() {
		return tracks.size();
	}

	public boolean isEmpty() {
		return tracks.isEmpty();
	}

	public boolean contains(Object o) {
		return tracks.contains(o);
	}

	public int indexOf(Object o) {
		return tracks.indexOf(o);
	}

	public int lastIndexOf(Object o) {
		return tracks.lastIndexOf(o);
	}

	public Object[] toArray() {
		return tracks.toArray();
	}

	public <T> T[] toArray(T[] a) {
		return tracks.toArray(a);
	}

	public Track get(int index) {
		return tracks.get(index);
	}

	public Track set(int index, Track element) {
		return tracks.set(index, element);
	}

	public boolean add(Track e) {
		return tracks.add(e);
	}

	public boolean remove(Object o) {
		return tracks.remove(o);
	}

	public void add(int index, Track element) {
		tracks.add(index, element);
	}

	public Track remove(int index) {
		return tracks.remove(index);
	}

	public void clear() {
		tracks.clear();
	}

	public boolean containsAll(Collection<?> c) {
		return tracks.containsAll(c);
	}

	public boolean addAll(Collection<? extends Track> c) {
		return tracks.addAll(c);
	}

	public boolean removeAll(Collection<?> c) {
		return tracks.removeAll(c);
	}

	public boolean retainAll(Collection<?> c) {
		return tracks.retainAll(c);
	}

	public boolean addAll(int index, Collection<? extends Track> c) {
		return tracks.addAll(index, c);
	}

	public List<Track> subList(int fromIndex, int toIndex) {
		return tracks.subList(fromIndex, toIndex);
	}

	public ListIterator<Track> listIterator(int index) {
		return tracks.listIterator(index);
	}

	public ListIterator<Track> listIterator() {
		return tracks.listIterator();
	}

	public Iterator<Track> iterator() {
		return tracks.iterator();
	}

}

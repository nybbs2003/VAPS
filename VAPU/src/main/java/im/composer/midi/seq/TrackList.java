package im.composer.midi.seq;
import java.util.AbstractList;

import javax.sound.midi.MidiEvent;
import javax.sound.midi.Track;


public class TrackList extends AbstractList<MidiEvent> {

	private final Track track;
	public TrackList(Track track) {
		this.track = track;
	}

	@Override
	public boolean add(MidiEvent e) {
		return track.add(e);
	}

	@Override
	public MidiEvent get(int index) {
		return track.get(index);
	}

	@Override
	public int size() {
		return track.size();
	}

	@Override
	public boolean remove(Object o) {
		if(o instanceof MidiEvent){
			return track.remove((MidiEvent) o);
		}else{
			return false;
		}
	}

	public long ticks() {
		return track.ticks();
	}

}

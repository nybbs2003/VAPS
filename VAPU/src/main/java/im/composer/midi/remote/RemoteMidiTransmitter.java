package im.composer.midi.remote;


import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import javax.sound.midi.Receiver;
import javax.sound.midi.Transmitter;

public class RemoteMidiTransmitter extends UnicastRemoteObject implements RemoteRawMidiListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6282396681588504415L;
	private Receiver receiver = null;
	private Transmitter tramsmitter = null;
	public RemoteMidiTransmitter() throws RemoteException {
	}

	public void setReceiver(Receiver receiver)  throws RemoteException {
		this.receiver = receiver;
	}

	public Receiver getReceiver() throws RemoteException  {
		return receiver;
	}

	@Override
	public void rawMidiMessage(byte[] data, long timeStamp) throws RemoteException {
		receiver.send(new RawMidiMessage(data), timeStamp);
	}

	public Transmitter asTransmitter(){
		if(tramsmitter==null){
			tramsmitter = new Transmitter(){
				private Receiver receiver = null;
				@Override
				public void setReceiver(Receiver receiver) {
					this.receiver=receiver;
				}

				@Override
				public Receiver getReceiver() {
					return receiver;
				}

				@Override
				public void close() {

				}};
		}
		return tramsmitter;
	}
}

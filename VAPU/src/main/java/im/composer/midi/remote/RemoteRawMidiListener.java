package im.composer.midi.remote;

import java.rmi.Remote;
import java.rmi.RemoteException;

import themidibus.listener.MidiListener;

public interface RemoteRawMidiListener extends Remote, MidiListener {
	public void rawMidiMessage(byte[] data,long timeStamp) throws RemoteException;
}

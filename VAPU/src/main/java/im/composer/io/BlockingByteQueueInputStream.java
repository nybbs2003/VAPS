package im.composer.io;

import java.io.IOException;
import java.io.InputStream;

public class BlockingByteQueueInputStream extends InputStream {

	private final BlockingByteQueueOutputStream out;

	public BlockingByteQueueInputStream(BlockingByteQueueOutputStream out) {
		this.out = out;
	}

	@Override
	public int available() throws IOException {
		if (out.closed && out.q.isEmpty()) {
			return -1;
		}
		return out.q.size();
	}

	@Override
	public int read() throws IOException {
		try {
			return out.q.take();
		} catch (InterruptedException e) {
			throw new IOException(e);
		}
	}

}

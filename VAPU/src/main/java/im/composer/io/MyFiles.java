package im.composer.io;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;

public class MyFiles {

	public static final boolean supportsRandomAccess(Path path) {
		if (path == null) {
			return false;
		}
		if (path.toUri().getScheme().equalsIgnoreCase("file")) {
			return true;
		}
		try {
			Map<String, Object> map = Files.readAttributes(path, "zdl:random_access");
			if (map == null) {
				return false;
			}
			return Boolean.parseBoolean(String.valueOf(map.get("zdl:random_access")));
		} catch (IOException e) {
			return false;
		}
	}

	public static final InputStream getEnhancedInputStream(Path path) throws IOException {
		if (supportsRandomAccess(path)) {
			return SkippingEnhancedInputStream.build(path);
		} else {
			InputStream in = path.getFileSystem().provider().newInputStream(path);
			if (!in.markSupported()) {
				in = new BufferedInputStream(in);
			}
			return in;
		}
	}

}

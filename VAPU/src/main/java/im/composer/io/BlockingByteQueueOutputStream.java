package im.composer.io;

import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.LinkedBlockingQueue;

public class BlockingByteQueueOutputStream extends OutputStream {

	protected final LinkedBlockingQueue<Byte> q = new LinkedBlockingQueue<Byte>();
	protected boolean closed = false;

	@Override
	public void close() throws IOException {
		closed = true;
	}

	@Override
	public void write(int b) throws IOException {
		try {
			q.put((byte) (b & 0xFF));
		} catch (InterruptedException e) {
			throw new IOException(e);
		}
	}

}

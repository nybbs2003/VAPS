package im.composer.generators;
import im.composer.audio.engine.Out;

public class RandOut extends Out {

	protected float gain = 1;

	public void setGain(float g) {
		this.gain = g;
	}

	protected void computeBuffer() {
		for (int i = 0; i < buf.getChannelCount(); i++) {
			float[] samples = buf.getChannel(i);
			for (int j = 0; j < bufferSize; j++) {
				double x = Math.random();
				samples[j] = (float) (gain * (2 * x - 1));
			}
		}
	}

}

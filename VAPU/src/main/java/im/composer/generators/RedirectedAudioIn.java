package im.composer.generators;

import im.composer.audio.engine.Out;

import org.jaudiolibs.audioservers.AudioConfiguration;
import org.tritonus.share.sampled.FloatSampleBuffer;

public class RedirectedAudioIn extends Out {
	
	private int channels;

	public void setChannels(int channels) {
		this.channels = channels;
	}

	@Override
	public synchronized void configure(AudioConfiguration context) throws Exception {
		super.configure(new AudioConfiguration(context.getSampleRate(), 0, channels, context.getMaxBufferSize(), context.isFixedBufferSize()));
	}

	public FloatSampleBuffer getBuffer(){
		return buf;
	}
	
	@Override
	protected void computeBuffer() {

	}

}

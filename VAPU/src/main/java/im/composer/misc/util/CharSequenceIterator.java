package im.composer.misc.util;

import java.util.Iterator;

public class CharSequenceIterator implements Iterable<Character>, Iterator<Character> {

	private final CharSequence seq;
	private int pos = 0;

	public CharSequenceIterator(CharSequence seq) {
		this.seq = seq;
	}

	@Override
	public Iterator<Character> iterator() {
		return this;
	}

	@Override
	public boolean hasNext() {
		return pos<seq.length();
	}

	@Override
	public Character next() {
		pos++;
		return seq.charAt(pos-1);
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException();
	}

}

package im.composer.persistent.xstream.converter;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Properties;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class PropertiesConverter implements Converter {

	@Override
	public boolean canConvert(@SuppressWarnings("rawtypes") Class arg0) {
		return Properties.class.isAssignableFrom(arg0);
	}

	@Override
	public void marshal(Object arg0, HierarchicalStreamWriter arg1, MarshallingContext arg2) {
		Properties p = (Properties) arg0;
		StringWriter writer = new StringWriter();
		try {
			p.store(writer, null);
		} catch (IOException e) {
		}
		arg1.setValue(writer.toString());
	}

	@Override
	public Object unmarshal(HierarchicalStreamReader arg0, UnmarshallingContext arg1) {
		Properties p = new Properties();
		try {
			p.load(new StringReader(arg0.getValue()));
		} catch (IOException e) {
		}
		return p;
	}

}

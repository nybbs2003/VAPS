
import im.composer.audioservers.adapter.CallableAudioServer;
import im.composer.media.sound.render.SourcePlayer;
import im.composer.midi.MidiCable;
import im.composer.vapu.adapter.GervillSynth;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.sound.midi.ShortMessage;

import org.jaudiolibs.audioservers.AudioConfiguration;
import org.jaudiolibs.audioservers.AudioServer;
import org.jaudiolibs.audioservers.javasound.JSAudioServerProvider;

import vapu.examples.MidiDebug;
import vapu.examples.ddelay.DDelay;

public class Test3 {

	/**
	 * @param args
	 * @throws Throwable
	 */
	public static void main(String[] args) throws Throwable {
		AudioConfiguration conf = new AudioConfiguration(48000, 1, 1, 512, true);
		MidiDebug mbd = new MidiDebug();
		GervillSynth synth = new GervillSynth();
		mbd.setReceiver(synth);
		DDelay delay = new DDelay();
		SourcePlayer player = new SourcePlayer();
		delay.addSource(synth);
		player.addSource(delay);
		MidiCable cable = new MidiCable("iCON iKEY V2.03", mbd);
		AudioServer server = new JSAudioServerProvider().createServer(conf, player);
		ExecutorService exec = Executors.newCachedThreadPool();
		CallableAudioServer.submit(exec, server);
		System.out.println("ready!");
		Thread.sleep(500);
		mbd.send(new ShortMessage(ShortMessage.NOTE_ON,0,0,1), 0);
		mbd.send(new ShortMessage(ShortMessage.NOTE_OFF,0,0,0), 0);
	}

}

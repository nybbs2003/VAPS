import java.util.Arrays;

import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiSystem;

import im.composer.midi.MidiCable;
import vapu.examples.MidiDisplay;

public class Main {

	public static void main(String[] args) throws Throwable{
		MidiDevice.Info[] infos = MidiSystem.getMidiDeviceInfo();
		for (MidiDevice.Info i : infos) {
			MidiDevice dev = MidiSystem.getMidiDevice(i);
			System.out.println(Arrays.asList(i.getName(),i.getDescription(),dev.getMaxTransmitters(),dev.getMaxReceivers()));
		}
		String dev_name ="WORLDE Panda MINI";
		new MidiCable(dev_name, new MidiDisplay());

		while(true){
			Thread.sleep(1000);
		}
	}

}

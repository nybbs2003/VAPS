package vapu.examples;
import im.composer.audioservers.adapter.CallableAudioServer;
//import im.composer.audioservers.asio.AsioAudioServer;
import im.composer.generators.Sine;
import im.composer.media.sound.render.SourcePlayer;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.jaudiolibs.audioservers.AudioConfiguration;
import org.jaudiolibs.audioservers.AudioServer;
import org.jaudiolibs.audioservers.javasound.JSAudioServerProvider;


public class SineExample {

	/**
	 * @param args
	 * @throws Throwable 
	 */
	public static void main(String[] args) throws Throwable {
		AudioConfiguration conf = new AudioConfiguration(48000, 0, 1, 1024, true);
		final Sine sin = new Sine();
		SourcePlayer player = new SourcePlayer();
		player.addSource(sin);
		AudioServer server = new JSAudioServerProvider().createServer(conf, player);
//		AudioServer server = AsioAudioServer.createServer(conf, player);
		ExecutorService exec = Executors.newCachedThreadPool();
		CallableAudioServer.submit(exec, server);
		System.out.println("ready!");
		
		//周期性改变频率
		exec.execute(new Runnable(){

			float f = 330;
			double ratio = 500;
			@Override
			public void run() {
				while (true) {
					long t = System.currentTimeMillis();
					sin.setFrequency((float) (f * (Math.sin(t / ratio)+2)));
					try {
						Thread.sleep(5);
					} catch (InterruptedException e) {
					}
				}
			}});
	}

}

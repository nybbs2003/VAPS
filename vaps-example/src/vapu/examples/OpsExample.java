package vapu.examples;

import im.composer.audioservers.adapter.CallableAudioServer;
import im.composer.generators.RedirectedAudioIn;
import im.composer.media.sound.render.SourcePlayer;
import im.composer.vapu.adapter.op.OpAdapter;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.jaudiolibs.audioops.impl.FreeverbOp;
import org.jaudiolibs.audioops.impl.GainOp;
import org.jaudiolibs.audioservers.AudioConfiguration;
import org.jaudiolibs.audioservers.AudioServer;
import org.jaudiolibs.audioservers.javasound.JSAudioServerProvider;

import vapu.examples.rmi.Limiter;

public class OpsExample {

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Throwable {
		AudioConfiguration conf = new AudioConfiguration(48000, 1, 1, 1024, true);
		OpAdapter adp1 = new OpAdapter(new GainOp(2));
		Limiter limiter = new Limiter();
		limiter.setUpper(0.99995f);
		limiter.setLower(0.0001f);
		OpAdapter adp2 = new OpAdapter(new FreeverbOp());
		SourcePlayer player = new SourcePlayer();
		player.configure(conf);
		RedirectedAudioIn in = player.createAudioIn(0);
		limiter.addSource(in);
		adp1.addSource(limiter);
		adp2.addSource(adp1);
		player.addSource(adp2);
		AudioServer server = new JSAudioServerProvider().createServer(conf, player);
//		AudioServer server = AsioAudioServer.createServer(conf, player);
		ExecutorService exec = Executors.newCachedThreadPool();
		CallableAudioServer.submit(exec, server);
		System.out.println("ready!");
	}

}

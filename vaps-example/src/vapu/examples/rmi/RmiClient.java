package vapu.examples.rmi;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import im.composer.audioservers.adapter.CallableAudioServer;
import im.composer.audioservers.rmi.RmiAudioServer;
import im.composer.generators.Sine;
import im.composer.media.sound.render.SourcePlayer;

import org.jaudiolibs.audioservers.AudioConfiguration;

public class RmiClient {

	/**
	 * @param args
	 * @throws Throwable
	 */
	public static void main(String[] args) throws Throwable {
		AudioConfiguration audioContext = new AudioConfiguration(48000, 1, 1, 1024);
		SourcePlayer player = new SourcePlayer();
		Sine sin = new Sine();
		player.addSource(sin);
		RmiAudioServer server = RmiAudioServer.create("192.168.0.111", "terminal_1", audioContext, player);
//		ExecutorService exec = Executors.newCachedThreadPool();
//		CallableAudioServer.submit(exec, server);
//		System.out.println("running!");
		server.run();
	}

}

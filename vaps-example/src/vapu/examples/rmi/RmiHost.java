package vapu.examples.rmi;

import im.composer.audioservers.adapter.CallableAudioServer;
import im.composer.audioservers.rmi.RmiAudioAdapter;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.jaudiolibs.audioservers.AudioClient;
import org.jaudiolibs.audioservers.AudioConfiguration;
import org.jaudiolibs.audioservers.AudioServer;
import org.jaudiolibs.audioservers.javasound.JSAudioServerProvider;

public class RmiHost {

	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		AudioConfiguration conf = new AudioConfiguration(48000, 1, 1, 1024, true);
		AudioClient client = RmiAudioAdapter.lookup("localhost","terminal_1");
		AudioServer server = new JSAudioServerProvider().createServer(conf, client);
//		ExecutorService exec = Executors.newCachedThreadPool();
//		CallableAudioServer.submit(exec, server);
//		System.out.println("ready!");
		server.run();
	}

}

package vapu.examples;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiSystem;

import org.jaudiolibs.audioservers.AudioConfiguration;
import org.jaudiolibs.audioservers.AudioServer;
import org.jaudiolibs.audioservers.javasound.JSAudioServerProvider;

import im.composer.audioservers.adapter.CallableAudioServer;
import im.composer.media.sound.render.SourcePlayer;
import im.composer.midi.MidiCable;
import im.composer.vapu.VAPU;
import vapu.examples.ddelay.DDelay;
import vapu.examples.liquinth.LiquinthVST;

public class SynthExample {

	public static void main(String[] args) throws Throwable {
		MidiDevice.Info[] infos = MidiSystem.getMidiDeviceInfo();
		for (MidiDevice.Info i : infos) {
			MidiDevice dev = MidiSystem.getMidiDevice(i);
			System.out.println(Arrays.asList(i.getName(),i.getDescription(),dev.getMaxTransmitters(),dev.getMaxReceivers()));
		}

		
		
		AudioConfiguration conf = new AudioConfiguration(48000, 2, 2, 512, true);
		VAPU synth;
//		synth = new GervillSynth();
//		synth = new DreiNullDrei();
		synth = new LiquinthVST();
		DDelay delay = new DDelay();
		delay.addSource(synth);
		SourcePlayer player = new SourcePlayer();
		player.addSource(delay);
		String dev_name ="WORLDE Panda MINI";
//		new MidiCable(dev_name, new MidiDebug());
		new MidiCable(dev_name, new MidiDisplay());
		new MidiCable(dev_name, delay);
		new MidiCable(dev_name, synth);
		AudioServer server = new JSAudioServerProvider().createServer(conf, player);
//		AudioServer server = AsioAudioServer.createServer(conf, player);
		ExecutorService exec = Executors.newCachedThreadPool();
		CallableAudioServer.submit(exec, server);
		System.out.println("ready!");
		Thread.sleep(500);
	}

}

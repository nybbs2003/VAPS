package vapu.examples.jaydelay;

import im.composer.audioservers.adapter.CallableAudioServer;
import im.composer.generators.RedirectedAudioIn;
import im.composer.media.sound.render.SourcePlayer;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.jaudiolibs.audioservers.AudioConfiguration;
import org.jaudiolibs.audioservers.AudioServer;
import org.jaudiolibs.audioservers.javasound.JSAudioServerProvider;

public class JayMain {

	/**
	 * @param args
	 * @throws Throwable 
	 */
	public static void main(String[] args) throws Throwable {
		AudioConfiguration conf = new AudioConfiguration(48000, 2, 2, 1024, true);
		JayDLay delay = new JayDLay();
		SourcePlayer player = new SourcePlayer();
		player.configure(conf);
		RedirectedAudioIn in = player.createAudioIn(0,1);
		delay.addSource(in);
		player.addSource(delay);
		AudioServer server = new JSAudioServerProvider().createServer(conf, player);
		ExecutorService exec = Executors.newCachedThreadPool();
		CallableAudioServer.submit(exec, server);
		System.out.println("ready!");
	}

}

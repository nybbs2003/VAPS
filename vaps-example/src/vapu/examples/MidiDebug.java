package vapu.examples;

import im.composer.vapu.VAPU;

import javax.sound.midi.MidiMessage;

public class MidiDebug extends VAPU {

	@Override
	protected void executeCommand(long timeStamp, String command) {
		// TODO Auto-generated method stub

	}

	@Override
	public void send(MidiMessage message, long timeStamp) {
		StringBuilder sb = new StringBuilder(512);
		sb.append(timeStamp);
		sb.append(" , ");
		for(byte b:message.getMessage()){
			sb.append(Integer.toHexString(Byte.toUnsignedInt(b)).toUpperCase());
			sb.append(' ');
		}
		System.out.println(sb);
		if(getReceiver()!=null){
			getReceiver().send(message, timeStamp);
		}
	}

	@Override
	protected void computeBuffer() {
		// TODO Auto-generated method stub

	}

}

package vapu.examples;

import java.util.Arrays;

import im.composer.vapu.VAPU;
import themidibus.listener.SimpleMidiListener;

public class MidiDisplay extends VAPU implements SimpleMidiListener {

	@Override
	public void activeSensing(long timeStamp) {
		System.out.println("activeSensing "+timeStamp);
	}

	@Override
	public void noteOn(int channel, int pitch, int velocity) {
		System.out.println("noteOn "+Arrays.asList(channel,pitch,velocity));
	}

	@Override
	public void noteOff(int channel, int pitch, int velocity) {
		System.out.println("noteOff "+Arrays.asList(channel,pitch,velocity));
	}

	@Override
	public void controllerChange(int channel, int number, int value) {
		System.out.println("controllerChange "+Arrays.asList(channel,number,value));
	}

	@Override
	public void programChange(int channel, int value) {
		System.out.println("programChange "+Arrays.asList(channel,value));
	}

	@Override
	public void pitchBend(int channel, float val) {
		System.out.println("pitchBend "+Arrays.asList(channel,val));
	}

	@Override
	public void channelPressure(int channel, float val) {
		System.out.println("channelPressure "+Arrays.asList(channel,val));
	}

	@Override
	public void channelVolume(int channel, float val) {
		System.out.println("channelVolume "+Arrays.asList(channel,val));
	}

	@Override
	public void sustainChange(boolean b) {
		System.out.println("sustainChange "+Arrays.asList(b));
	}

	@Override
	protected void executeCommand(long timeStamp, String command) {
		
	}

	@Override
	protected void computeBuffer() {
		
	}


}

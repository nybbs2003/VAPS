package vapu.examples.gziprmi;

import im.composer.audioservers.adapter.CallableAudioServer;
import im.composer.audioservers.rmi.comporessed.GzipRmiAudioServer;
import im.composer.generators.Sine;
import im.composer.media.sound.render.SourcePlayer;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class RmiClient {

	public static void main(String[] args) throws Throwable {
		SourcePlayer player = new SourcePlayer();
		Sine sin = new Sine();
		player.addSource(sin);
		GzipRmiAudioServer server = new GzipRmiAudioServer(player, "192.168.0.111", 1099, "terminal_2");
		ExecutorService exec = Executors.newCachedThreadPool();
		CallableAudioServer.submit(exec, server);
		System.out.println("running!");
	}

}

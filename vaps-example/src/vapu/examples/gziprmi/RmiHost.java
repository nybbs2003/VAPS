package vapu.examples.gziprmi;

import im.composer.audioservers.adapter.CallableAudioServer;
import im.composer.audioservers.rmi.comporessed.GzipRmiAudioAdapter;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.jaudiolibs.audioservers.AudioClient;
import org.jaudiolibs.audioservers.AudioConfiguration;
import org.jaudiolibs.audioservers.AudioServer;
import org.jaudiolibs.audioservers.javasound.JSAudioServerProvider;

public class RmiHost {

	public static void main(String[] args) throws Exception {
		AudioConfiguration conf = new AudioConfiguration(48000, 1, 1, 1024, true);
		AudioClient client = GzipRmiAudioAdapter.lookup("192.168.0.111","terminal_2");
		AudioServer server = new JSAudioServerProvider().createServer(conf, client);
		ExecutorService exec = Executors.newCachedThreadPool();
		CallableAudioServer.submit(exec, server);
		System.out.println("ready!");
	}

}

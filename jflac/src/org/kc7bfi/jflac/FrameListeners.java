/*
 * Created on Jun 28, 2004
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package org.kc7bfi.jflac;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.kc7bfi.jflac.frame.Frame;
import org.kc7bfi.jflac.metadata.Metadata;

/**
 * Class to handle frame listeners.
 * 
 * @author kc7bfi
 */
public class FrameListeners implements FrameListener {

	private final List<FrameListener> list = Collections.synchronizedList(new LinkedList<FrameListener>());

	/**
	 * Add a frame listener.
	 * 
	 * @param listener
	 *            The frame listener to add
	 */
	public void addFrameListener(FrameListener listener) {
		list.add(listener);
	}

	/**
	 * Remove a frame listener.
	 * 
	 * @param listener
	 *            The frame listener to remove
	 */
	public void removeFrameListener(FrameListener listener) {
		list.remove(listener);
	}

	/**
	 * Process metadata records.
	 * 
	 * @param metadata
	 *            the metadata block
	 * @see org.kc7bfi.jflac.FrameListener#processMetadata(org.kc7bfi.jflac.metadata.MetadataBase)
	 */
	public void processMetadata(Metadata metadata) {
		list.stream().forEach(o -> o.processMetadata(metadata));
	}

	/**
	 * Process data frames.
	 * 
	 * @param frame
	 *            the data frame
	 * @see org.kc7bfi.jflac.FrameListener#processFrame(org.kc7bfi.jflac.frame.Frame)
	 */
	public void processFrame(Frame frame) {
		list.stream().forEach(o -> o.processFrame(frame));
	}

	/**
	 * Called for each frame error detected.
	 * 
	 * @param msg
	 *            The error message
	 * @see org.kc7bfi.jflac.FrameListener#processError(java.lang.String)
	 */
	public void processError(String msg) {
		list.stream().forEach(o -> o.processError(msg));
	}

}

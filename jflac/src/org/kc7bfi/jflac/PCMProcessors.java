/*
 * Created on Jun 28, 2004
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package org.kc7bfi.jflac;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.kc7bfi.jflac.metadata.StreamInfo;
import org.kc7bfi.jflac.util.ByteData;

/**
 * Class to handle PCM processors.
 * 
 * @author kc7bfi
 */
public class PCMProcessors implements PCMProcessor {
	private List<PCMProcessor> pcmProcessors = Collections.synchronizedList(new LinkedList<PCMProcessor>());

	/**
	 * Add a PCM processor.
	 * 
	 * @param processor
	 *            The processor listener to add
	 */
	public void addPCMProcessor(PCMProcessor processor) {
		pcmProcessors.add(processor);
	}

	/**
	 * Remove a PCM processor.
	 * 
	 * @param processor
	 *            The processor listener to remove
	 */
	public void removePCMProcessor(PCMProcessor processor) {
		pcmProcessors.remove(processor);
	}

	/**
	 * Process the StreamInfo block.
	 * 
	 * @param info
	 *            the StreamInfo block
	 * @see org.kc7bfi.jflac.PCMProcessor#processStreamInfo(org.kc7bfi.jflac.metadata.StreamInfo)
	 */
	public void processStreamInfo(StreamInfo info) {
		pcmProcessors.stream().forEach(o -> o.processStreamInfo(info));
	}

	/**
	 * Process the decoded PCM bytes.
	 * 
	 * @param pcm
	 *            The decoded PCM data
	 * @see org.kc7bfi.jflac.PCMProcessor#processPCM(org.kc7bfi.jflac.util.ByteSpace)
	 */
	public void processPCM(ByteData pcm) {
		pcmProcessors.stream().forEach(o -> o.processPCM(pcm));
	}

}

package im.composer.audioservers.rmi;

import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;

/**
 * 
 * @author David Zhang (zdl@zdl.hk)
 * @since 2013-4-14
 */
public class AudioTerminalServer {

	private String name;
	private AudioTerminal terminal;
	private String host = null;
	private int port;

	public AudioTerminalServer(String name, AudioTerminal terminal, String host, int port) {
		this.name = name;
		this.terminal = terminal;
		this.host = host;
		this.port = port;
	}

	public AudioTerminal getTerminal() {
		return terminal;
	}

	public void bind() throws AccessException, RemoteException {
		try {
			LocateRegistry.createRegistry(port);
		} catch (Exception e) {
		}
		LocateRegistry.getRegistry(host,port).rebind(name, terminal);
	}

	public void unbind() throws AccessException, RemoteException, NotBoundException {
		LocateRegistry.getRegistry(host,port).unbind(name);
	}
}

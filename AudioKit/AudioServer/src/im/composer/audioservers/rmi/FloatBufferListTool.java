package im.composer.audioservers.rmi;

import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;

public class FloatBufferListTool {

	public static final List<FloatBuffer> convertArr2Buf(List<float[]> l_arr) {
		List<FloatBuffer> l_buf = new ArrayList<>(l_arr.size());
		for (int i = 0; i < l_arr.size(); i++) {
			float[] arr = l_arr.get(i);
			FloatBuffer buf = FloatBuffer.wrap(arr);
			l_buf.add(buf);
		}
		return l_buf;
	}
	
	public static final List<float[]> convertBuf2Arr(List<FloatBuffer> l_buf){
		List<float[]> l_arr = new ArrayList<>(l_buf.size());
		for (int i = 0; i < l_buf.size(); i++) {
			FloatBuffer buf = l_buf.get(i);
			float[] arr = buf.array();
			l_arr.add(arr);
		}
		return l_arr;
	}
}

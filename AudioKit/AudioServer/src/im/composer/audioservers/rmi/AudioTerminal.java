package im.composer.audioservers.rmi;

import java.nio.FloatBuffer;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

import org.jaudiolibs.audioservers.AudioConfiguration;

public interface AudioTerminal extends Remote {

	public AudioTerminal getPeer() throws RemoteException;

	public void setPeer(AudioTerminal peer) throws RemoteException;

	public void configure(AudioConfiguration context) throws RemoteException;

	public void audioReceived(long time, List<FloatBuffer> channels, int nframes) throws RemoteException;
	/**
	 * 
	 * @param time System.nano() 相对时间
	 * @param data 音频输入
	 * @param nframes 帧数
	 * @return 音频输出
	 */
	public List<float[]> audioExchange(long time, List<float[]> data, int nframes) throws RemoteException;
}

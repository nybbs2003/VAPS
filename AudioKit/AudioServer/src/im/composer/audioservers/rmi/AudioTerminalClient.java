package im.composer.audioservers.rmi;

import java.nio.FloatBuffer;
import java.rmi.RemoteException;
import java.util.List;

import org.jaudiolibs.audioservers.AudioClient;
import org.jaudiolibs.audioservers.AudioConfiguration;

public class AudioTerminalClient implements AudioClient {

	private final AudioTerminal terminal;

	public AudioTerminalClient(AudioTerminal terminal) {
		super();
		this.terminal = terminal;
	}

	@Override
	public void configure(AudioConfiguration context) throws Exception {
		terminal.configure(context);
	}

	@Override
	public boolean process(long time, List<FloatBuffer> inputs, List<FloatBuffer> outputs, int nframes) {
		List<float[]> result;
		try {
			result = terminal.audioExchange(time, FloatBufferListTool.convertBuf2Arr(inputs), nframes);
		} catch (RemoteException e) {
			return false;
		}
		for(int i=0;i<result.size();i++){
			float[] a = result.get(i);
			float[] b = outputs.get(i).array();
			System.arraycopy(a, 0, b, 0, nframes);
		}
		return true;
	}

	@Override
	public void shutdown() {
		
	}
	
}

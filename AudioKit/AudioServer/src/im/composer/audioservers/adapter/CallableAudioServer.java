package im.composer.audioservers.adapter;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;

import org.jaudiolibs.audioservers.AudioConfiguration;
import org.jaudiolibs.audioservers.AudioServer;

public class CallableAudioServer implements Callable<Void>, AudioServer {

	private final AudioServer server;

	public CallableAudioServer(AudioServer server) {
		super();
		this.server = server;
	}

	@Override
	public Void call() throws Exception {
		server.run();
		return null;
	}

	public void run() throws Exception {
		server.run();
	}

	public AudioConfiguration getAudioContext() {
		return server.getAudioContext();
	}

	public boolean isActive() {
		return server.isActive();
	}

	public void shutdown() {
		server.shutdown();
	}

	public static final void submit(ExecutorService exec, AudioServer server) {
		exec.submit(new CallableAudioServer(server));
	}
}

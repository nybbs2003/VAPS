package im.composer.midi.ip;

import javax.sound.midi.Receiver;
import javax.sound.midi.Transmitter;

public class IPMidiTransmitter implements Transmitter {

	private Receiver receiver;

	public Receiver getReceiver() {
		return receiver;
	}

	public void setReceiver(Receiver receiver) {
		this.receiver = receiver;
	}

	@Override
	public void close() {
		
	}


}

package im.composer.midi.ip;

import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiDevice.Info;
import javax.sound.midi.spi.MidiDeviceProvider;

public class IPMidiDeviceProvider extends MidiDeviceProvider {

	@Override
	public MidiDevice getDevice(Info info) {
		for(MidiDevice dev:IPMidiDevice.devices){
			if(dev.getDeviceInfo().equals(info)){
				return dev;
			}
		}
		return null;
	}

	@Override
	public Info[] getDeviceInfo() {
		IPMidiDevice[] devices = IPMidiDevice.devices;
		Info[] infos = new Info[devices.length];
		for(int i=0;i<devices.length;i++){
			infos[i]=devices[i].getDeviceInfo();
		}
		return infos;
	}

}

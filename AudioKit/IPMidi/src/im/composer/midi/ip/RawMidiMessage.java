package im.composer.midi.ip;

import javax.sound.midi.MidiMessage;
public class RawMidiMessage extends MidiMessage implements java.io.Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 9074306730148706336L;

	public RawMidiMessage(byte[] data) {
		super(data);
	}

	@Override
	public RawMidiMessage clone() {
		return new RawMidiMessage(getMessage());
	}

}

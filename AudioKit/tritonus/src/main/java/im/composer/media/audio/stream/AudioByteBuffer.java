package im.composer.media.audio.stream;

import java.io.IOException;

public abstract class AudioByteBuffer extends LinkedByteArrayInputStream {

	@Override
	public int available() throws IOException {
		if (super.available() == 0) {
			requestNewBuffer();
		}
		return super.available();
	}

	public abstract void requestNewBuffer() throws IOException;
}

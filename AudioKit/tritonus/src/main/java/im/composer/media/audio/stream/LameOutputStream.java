package im.composer.media.audio.stream;

import java.io.IOException;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;

import org.tritonus.share.sampled.file.AudioOutputStream;
import org.tritonus.share.sampled.file.TDataOutputStream;

import lowlevel.LameEncoder;

public class LameOutputStream extends LameEncoder implements AudioOutputStream {

	protected final TDataOutputStream out;
	private byte[] buf = new byte[getPCMBufferSize()];

	public LameOutputStream(TDataOutputStream dataOutputStream) {
		this.out = dataOutputStream;
	}

	public AudioFormat getFormat() {
		return getSourceFormat();
	}

	public long getLength() {
		return AudioSystem.NOT_SPECIFIED;
	}

	@Override
	public void close() {
		int len = encodeFinish(buf);
		try {
			out.write(buf, 0, len);
		} catch (IOException e) {
		}
		super.close();
		try {
			out.close();
		} catch (IOException e) {
		}
	}

	public int write(byte[] abData, int nOffset, int nLength) throws IOException {
		int len = encodeBuffer(abData, nOffset, nLength, buf);
		out.write(buf, 0, len);
		return len;
	}
}

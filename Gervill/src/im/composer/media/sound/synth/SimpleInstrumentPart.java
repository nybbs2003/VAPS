package im.composer.media.sound.synth;

import java.util.Arrays;

public class SimpleInstrumentPart {
	private ModelPerformer[] performers;
	private int keyFrom;
	private int keyTo;
	private int velFrom;
	private int velTo;
	private int exclusiveClass;

	public ModelPerformer[] getPerformers() {
		return performers;
	}

	public void setPerformers(ModelPerformer[] performers) {
		this.performers = performers;
	}

	public int getKeyFrom() {
		return keyFrom;
	}

	public void setKeyFrom(int keyFrom) {
		this.keyFrom = keyFrom;
	}

	public int getKeyTo() {
		return keyTo;
	}

	public void setKeyTo(int keyTo) {
		this.keyTo = keyTo;
	}

	public int getVelFrom() {
		return velFrom;
	}

	public void setVelFrom(int velFrom) {
		this.velFrom = velFrom;
	}

	public int getVelTo() {
		return velTo;
	}

	public void setVelTo(int velTo) {
		this.velTo = velTo;
	}

	public int getExclusiveClass() {
		return exclusiveClass;
	}

	public void setExclusiveClass(int exclusiveClass) {
		this.exclusiveClass = exclusiveClass;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + exclusiveClass;
		result = prime * result + keyFrom;
		result = prime * result + keyTo;
		result = prime * result + Arrays.hashCode(performers);
		result = prime * result + velFrom;
		result = prime * result + velTo;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SimpleInstrumentPart other = (SimpleInstrumentPart) obj;
		if (exclusiveClass != other.exclusiveClass)
			return false;
		if (keyFrom != other.keyFrom)
			return false;
		if (keyTo != other.keyTo)
			return false;
		if (!Arrays.equals(performers, other.performers))
			return false;
		if (velFrom != other.velFrom)
			return false;
		if (velTo != other.velTo)
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SimpleInstrumentPart [");
		if (performers != null) {
			builder.append("performers=");
			builder.append(Arrays.toString(performers));
			builder.append(", ");
		}
		builder.append("keyFrom=");
		builder.append(keyFrom);
		builder.append(", keyTo=");
		builder.append(keyTo);
		builder.append(", velFrom=");
		builder.append(velFrom);
		builder.append(", velTo=");
		builder.append(velTo);
		builder.append(", exclusiveClass=");
		builder.append(exclusiveClass);
		builder.append("]");
		return builder.toString();
	}
}
